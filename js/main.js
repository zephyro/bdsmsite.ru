$( document ).ready(function() {

    $(function() {
        var pull = $('.nav-toggle');
        var menu = $('.header-nav');

        $(pull).on('click', function(e) {
            e.preventDefault();
            pull.toggleClass('open');
            menu.toggleClass('open');
        });
    });

    // Select

    $(function() {

        $('.main-select-selected').on('click touchstart', function(e) {
            e.preventDefault();

            if($(this).closest('.main-select').hasClass('open')){
                $(this).closest('.main-select').toggleClass('open');
            }
            else {
                $('.main-select').removeClass('open');
                $(this).closest('.main-select').toggleClass('open');
            }
        });

        $('.main-select-dropdown label').on('click touchstart', function(e){
            console.log('change');
            $(".main-select").removeClass('open');
            var MyBox = $(this).closest('.main-select');
            var MyTxt = $(this).find('input').val();
            MyBox.find('.main-select-value').text(MyTxt);
        });

        $('body').click(function (event) {
            if ($(event.target).closest(".main-select").length === 0) {
                $(".main-select").removeClass('open');
            }
        });

    });


    $(".btn-modal").fancybox({
        'padding'    : 0
    });


    $(".btn-modal-close").on('click touchstart', function(e){
        $.fancybox.close(true);
    });



    $('.people-slider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        focusOnSelect: true,
        prevArrow: '<span class="slide-nav prev"><i class="fa fa-angle-left"></i></span>',
        nextArrow: '<span class="slide-nav next"><i class="fa fa-angle-right"></i></span>',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            }
        ]
    });

    // Forms

    $('select').selectric({
        disableOnMobile: false,
        responsive: true,
        maxHeight: 250
    });


    // Dropdown button

    $('.btn-dropdown > a').on('click touchstart', function(e){
        e.preventDefault();
        var MyBox = $(this).closest('.btn-dropdown');
        MyBox.toggleClass('open');

    });

    // Usertags

    $('.usertags-group li').click(function(e) {
        e.preventDefault();
        var box = $(this).closest('.usertags-new').find('.usertags-category');
        var tab = $($(this).attr("data-type"));

        console.log(tab);
        $(this).closest('.usertags-group').find('li').removeClass('active');
        $(this).addClass('active');

        box.find('ul').removeClass('active');
        box.find(tab).addClass('active');

    });


    $('.usertag-add').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.usertags-new').addClass('open');
    });


    $('.btn-usertag-add').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.usertags-new').removeClass('open');
    });


    $(function() {

        $('.btn-add-question').on('click touchstart', function(e){
            e.preventDefault();
            $(this).closest('.profile-question').addClass('add-question');
        });

        $('.question-cancel').on('click touchstart', function(e){
            // e.preventDefault();
            $(this).closest('.profile-question').removeClass('add-question');
        });


        $('.btn-question-upload').on('click touchstart', function(e){
            e.preventDefault();
            var txt = $(this).closest('.profile-question-new').find('textarea').val();
            var box = $(this).closest('.profile-question').find('.profile-question-content');
            var str = '<div class="question-item">' + txt + '<span class="question-item-delete"><i class="fa fa-close"></i></span></div>';
            console.log(txt);
            box.append(str);
            $(this).closest('.profile-question').find('.question-intro').hide();
            $(this).closest('.profile-question-new').find('textarea').val('');
            $(this).closest('.profile-question').removeClass('add-question');
        });


        $(".question-item-delete").on("click", "remove", function (e) {
            alert('you clicked me!');
        });
    });

    $(function() {

        $('.btn-data-edit').on('click touchstart', function(e){
            e.preventDefault();
            $(this).closest('.profile-box').addClass('add-data');
        });

        $('.profile-data-reset').on('click touchstart', function(e){
            // e.preventDefault();
            $(this).closest('.profile-box').removeClass('add-data');
        });

        $('.profile-data-submit').on('click touchstart', function(e){
            e.preventDefault();
            $(this).closest('.profile-box').removeClass('add-data');
        });

    });


    (function($) {

        $('.bg-list li a').on('click touchstart', function(e){
            e.preventDefault();
            $(this).closest('.bg-list').find('li').removeClass('active');
            $(this).closest('li').addClass('active');
            var bg = $(this).attr("data-value");
            console.log(bg);
            $('body').removeClassWild("theme_*");
            $('body').addClass(bg);
        });

        $.fn.removeClassWild = function(mask) {
            return this.removeClass(function(index, cls) {
                var re = mask.replace(/\*/g, '\\S+');
                return (cls.match(new RegExp('\\b' + re + '', 'g')) || []).join(' ');
            });
        };


    })(jQuery);

    // Payment


    $(".btn-modal-payment").fancybox({
        'padding'    : 0,
        modal   : true,
        onInit: function() {
            $('.payment-method').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                focusOnSelect: false,
                prevArrow: '<span class="slide-nav prev"><span></span></span>',
                nextArrow: '<span class="slide-nav next"><span></span></span>',
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3
                        }
                    }
                ]
            });
        },
        afterClose: function() {
            $('.payment-method').slick('unslick');
        }
    });



    var $pay = $("#payment-amount").ionRangeSlider({
        type: "single",
        values: [
            7, 30, 90, 180, 365
        ],
        grid: true,
        grid_snap: true,
        postfix: 'дней'
    });



    var $payPopular = $("#payment-amount-popular").ionRangeSlider({
        type: "single",
        values: [
            7, 30, 90, 180, 365
        ],
        grid: true,
        grid_snap: true,
        postfix: 'дней'
    });


    $pay.on("change", function () {
        var $this = $(this),
            pricePay = 0,
            priceCredit = 0,
            value = parseInt($this.prop("value"));

        switch (value) {
            case 7:
                pricePay = 100;
                priceCredit = 200;
                break;
            case 30:
                pricePay = 250;
                priceCredit = 500;
                break;
            case 90:
                pricePay = 600;
                priceCredit = 1200;
                break;
            case 180:
                pricePay = 1000;
                priceCredit = 2000;
                break;
            case 365:
                pricePay = 1800;
                priceCredit = 3600;
                break;
            default:
                console.log('error');
        }


        $('.payment').find('.price-value-credit').text(priceCredit);
        $('.payment').find('.price-value-pay').text(pricePay);
    });


    $payPopular.on("change", function () {
        var $this = $(this),
            pricePay = 0,
            priceCredit = 0,
            value = parseInt($this.prop("value"));

        switch (value) {
            case 7:
                pricePay = 100;
                priceCredit = 200;
                break;
            case 30:
                pricePay = 250;
                priceCredit = 500;
                break;
            case 90:
                pricePay = 600;
                priceCredit = 1200;
                break;
            case 180:
                pricePay = 1000;
                priceCredit = 2000;
                break;
            case 365:
                pricePay = 1800;
                priceCredit = 3600;
                break;
            default:
                console.log('error');
        }


        $('.payment').find('.price-value-credit').text(priceCredit);
        $('.payment').find('.price-value-pay').text(pricePay);
    });

    $('.payment-method-nav').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("data-target"));
        var box = $(this).closest('.payment');

        $(this).closest('.payment-method').find('.payment-method-nav').removeClass('active');
        $(this).addClass('active');

        box.find('.payment-content').removeClass('payment-enable');
        box.find(tab).addClass('payment-enable');
    });


    $('.payment-operator li span').on('click touchstart', function(e){
        e.preventDefault();
        var oss = $($(this).attr("data-target"));

        console.log(oss);

        $('.payment-operator').find('li').removeClass('enable');
        $(this).closest('li').addClass('enable');
    });


    $('.photoline-slider').slick({
        slidesToShow: 11,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 1000
    });


});




$('.tabs-nav > li > a').on('click touchstart', function(e){
    e.preventDefault();

    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tabs');

    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('> .tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});



$('.payment-type-content form').on("change", function () {
    var coin = $(this).find('input[type="radio"]:checked').val();
    var box =$(this).closest('.payment-type-content');

    console.log(coin);
    box.find('.method-result-value').text(coin);
});

