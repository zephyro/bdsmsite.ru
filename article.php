
<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">
                    <ul class="breadcrumbs">
                        <li><a href="#">Главная</a></li>
                        <li><a href="#">Статьи</a></li>
                        <li><a href="#">Психология</a></li>
                        <li>В БДСМ не все вербальные унижения допустимы</li>
                    </ul>

                    <div class="heading">
                        <h1>BDSM с девушкой: тонкости гендерной психологии</h1>
                        <ul class="article-meta">
                            <li>10.04.2017</li>
                            <li>BDSMSite.ru</li>
                            <li><a href="#">Общее</a></li>
                        </ul>
                    </div>

                    <div class="row">
                        <div class="col-sm-8 col-md-8 col-lg-9">
                            <div class="article-main">

                                <p>Понимание мужчины и женщины о власти и контроле зачастую сильно расходятся. Зато поняв, кто чего хочет на самом деле, можно обрести гармонию даже в рамках самой жесткой сессии.</p>
                                <p>Специально ли мужчины перегибают палку?</p>
                                <p>Он, вроде бы, и навредить ей не хочет, и уважает ее, но все равно во время минета крепко держит за голову и усердно пихает ей в глотку свой прибор. Нет, он вовсе не свинья, он просто думает, что так и надо, что именно это нравится его партнерше! И поэтому продолжает устраивать ей взбучку несмотря на очевидные звуки того, что она давится его членом.</p>
                                <p>Существует гипотеза, что женщинам нравится быть ведомыми в сексе. И она довольно правдоподобна.</p>
                                <p>Конечно же, женщина хочет, чтобы ее мужчина контролировал ее, ей нравится это ощущение беспомощности, неспособности остановить плотский голод самца. Женщины хотят, чтобы их брали и доставляли им невероятное удовольствие. Но иногда это работает немного не так.</p>
                                <p>К сожалению, многие мужчины понимают это лишь частично. Для многих доминировать – значит заставлять девушку делать то, чего она не хочет, например, заниматься сексом в почти нечеловеческой позе.</p>
                                <p>Чего женщины хотят на самом деле.</p>
                                <p>Вам когда-нибудь приходило в голову, что женщина имеет ввиду под словами “хочу, чтобы мужчина взял контроль”?</p>

                                <div class="article-box">
                                    <p>Материал на тему</p>
                                    <h4><a href="#">Практика воспитания сабмиссива  в БДСМ</a></h4>
                                    <div class="article-box-thumb">
                                        <a href="images/article_01.jpg" data-fancybox="gallery">
                                            <img src="images/article_01.jpg" class="img-responsive" alt="Практика воспитания сабмиссива  в БДСМ">
                                        </a>
                                    </div>
                                    <p>Моральная и сексуальная подготовка сабмиссива – настоящая миссия для Доминанта. Можно ли превратить процесс обучения нижней в ежедневную/еженедельную активность? </p>
                                </div>
                                <p>А вот что: ХОЧУ, ЧТОБЫ ОН ОБО МНЕ ЗАБОТИЛСЯ.</p>
                                <p>Контроль над женщиной не дает права на безответственность, а доминирование не означает неуважение. Контроль не должен быть жестоким или унизительным. Да, можно делать все, что угодно, но необходимо относится ко всему этому с ответственностью.</p>
                                <p>Она дает тебе контроль над собой потому что думает, что ты знаешь, что делаешь и поэтому дает тебе возможность убедить ее в этом. (Так зачем доказывать обратное?)</p>
                                <p>Как правильно Доминировать?</p>
                                <p>Если так получилось, что этой ночью вам предстоит побыть Доминантом, запомните одну очень важную вещь. Чтобы девушке понравилась ваша грубость и жесткость необходимо ее ХОРОШЕНЬКО ВОЗБУДИТЬ. Не просто возбудить, а сделать так, чтобы от желания у нее кружилась голова, а для этого нужно очень долго и сексуально ее дразнить. И вот когда она начнет сгорать от похоти, именно в этот момент можно начинать жесткий секс. Как ни странно, но путь к правильному Доминированию лежит именно через мягкость и нежность. И это очень долгий путь!</p>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-3">

                            <div class="article-thumb">
                                <a href="images/article_01.jpg" data-fancybox="gallery">
                                    <img src="images/article_01.jpg" class="img-responsive" alt="Практика воспитания сабмиссива  в БДСМ">
                                </a>
                            </div>

                            <div class="article-cat">
                                <div class="article-cat-title">
                                    <a href="#">Психология</a>
                                </div>
                                <ul class="article-cat-list">
                                    <li><a href="#">В БДСМ не все вербальные унижения допустимы</a></li>
                                    <li><a href="#">Puppy play и другие «дикие» игры БДСМ для взрослых!</a></li>
                                    <li><a href="#">БДСМ наклонности партнера можно развивать </a></li>
                                    <li><a href="#">Девушек в БДСМ с собаками роднят не только ошейник и цепь</a></li>
                                    <li><a href="#">Еще 45 материала</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>

                    <div class="related">
                        <h3 class="text-center">Похожие статьи</h3>
                        <ul class="related-list">
                            <li>
                                <a href="#">
                                    <img src="images/article_01.jpg" class="img-responsive" alt="">
                                    <div class="related-heading">В БДСМ не все вербальные унижения допустимы</div>
                                    <span class="related-seciton">Психология</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="images/article_02.jpg" class="img-responsive" alt="">
                                    <div class="related-heading">Практика воспитания сабмиссива  в БДСМ</div>
                                    <span class="related-seciton">Общее</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="images/article_03.jpg" class="img-responsive" alt="">
                                    <div class="related-heading">Как увлечь девушку сибари и связыванием</div>
                                    <span class="related-seciton">Личный опыть</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="images/article_04.jpg" class="img-responsive" alt="">
                                    <div class="related-heading">Как склонить девушку или парня к БДСМ</div>
                                    <span class="related-seciton">Психология</span>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <ul class="comments">
                        <li>
                            <div class="article-comments-author">
                                <div class="author-image">
                                    <img src="images/author_01.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="author-meta">
                                    <a href="#">User Name</a>
                                    <span>2017-02-23 15:39</span>
                                </div>
                            </div>
                            <div class="article-comments-content">
                                <p>Забавные наблюдения) Затрудняюсь сказать, на какую породу похожа моя девушка) Она любит выглядеть стильно. Ошейник предпочитает металлический, потому что он подходит под цвет любой одежды и прекрасно смотрится без нее. Цепь мы не используем, она тяжелая, заменяем поводком./</p>
                            </div>
                        </li>
                        <li>
                            <div class="article-comments-author">
                                <div class="author-image">
                                    <img src="images/author_01.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="author-meta">
                                    <a href="#">User Name</a>
                                    <span>2017-02-23 15:39</span>
                                </div>
                            </div>
                            <div class="article-comments-content">
                                <p>Забавные наблюдения) Затрудняюсь сказать, на какую породу похожа моя девушка) Она любит выглядеть стильно. Ошейник предпочитает металлический, потому что он подходит под цвет любой одежды и прекрасно смотрится без нее. Цепь мы не используем, она тяжелая, заменяем поводком./</p>
                            </div>

                            <ul>
                                <li>
                                    <div class="article-comments-author">
                                        <div class="author-image">
                                            <img src="images/author_01.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="author-meta">
                                            <a href="#">User Name</a>
                                            <span>2017-02-23 15:39</span>
                                        </div>
                                    </div>
                                    <div class="article-comments-content">
                                        <p>Забавные наблюдения) Затрудняюсь сказать, на какую породу похожа моя девушка) Она любит выглядеть стильно. Ошейник предпочитает металлический, потому что он подходит под цвет любой одежды и прекрасно смотрится без нее. Цепь мы не используем, она тяжелая, заменяем поводком./</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="article-comments-author">
                                        <div class="author-image">
                                            <img src="images/author_01.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="author-meta">
                                            <a href="#">User Name</a>
                                            <span>2017-02-23 15:39</span>
                                        </div>
                                    </div>
                                    <div class="article-comments-content">
                                        <p>Забавные наблюдения) Затрудняюсь сказать, на какую породу похожа моя девушка) Она любит выглядеть стильно. Ошейник предпочитает металлический, потому что он подходит под цвет любой одежды и прекрасно смотрится без нее. Цепь мы не используем, она тяжелая, заменяем поводком./</p>
                                    </div>
                                </li>
                            </ul>

                        </li>
                        <li>
                            <div class="article-comments-author">
                                <div class="author-image">
                                    <img src="images/author_01.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="author-meta">
                                    <a href="#">User Name</a>
                                    <span>2017-02-23 15:39</span>
                                </div>
                            </div>
                            <div class="article-comments-content">
                                <p>Забавные наблюдения) Затрудняюсь сказать, на какую породу похожа моя девушка) Она любит выглядеть стильно. Ошейник предпочитает металлический, потому что он подходит под цвет любой одежды и прекрасно смотрится без нее. Цепь мы не используем, она тяжелая, заменяем поводком./</p>
                            </div>
                        </li>
                    </ul>

                    <div class="omments-new">
                        <form class="form">
                            <div class="form-group">
                                <textarea class="form-control" name="message" placeholder="Ваш комментарий" rows="6"></textarea>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-md">Отправить</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
