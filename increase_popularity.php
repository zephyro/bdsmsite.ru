<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div class="box-border">

                        <div class="box-border-header no-bg">Когда у вас есть кредиты, можно делать классные вещи:</div>

                        <div class="payment-title">Выберите способ оплаты</div>

                        <div class="tabs">

                            <ul class="tabs-nav payment-nav">
                                <li class="active">
                                    <a href="#" data-target=".pay-mobile">
                                        <i class="fa fa-mobile"></i>
                                        <span>Мобильный счет</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-target=".pay-coin">
                                        <i class="fa fa-microchip"></i>
                                        <span>Электронные деньги</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-target=".pay-card">
                                        <i class="fa fa-credit-card"></i>
                                        <span>Кредитные карты</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-target=".pay-paypal">
                                        <i class="fa fa-paypal"></i>
                                        <span>PayPal</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tabs-item payment-type pay-mobile active">

                                <div class="tabs">

                                    <ul class="tabs-nav payment-type-nav payment-mobile-elem">
                                        <li class="active"><a href="#" data-target=".pay-mts">МТС</a></li>
                                        <li><a href="#" data-target=".pay-megafon">Мегафон</a></li>
                                        <li><a href="#" data-target=".pay-tele2">TELE2</a></li>
                                        <li><a href="#" data-target=".pay-beeline">Билайн</a></li>
                                    </ul>

                                    <div class="tabs-item payment-type-content pay-mts active">
                                        <form class="form">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="payment-selector">

                                                        <div class="payment-selector-title"><span>Сколько кредитов положить на счет?</span></div>

                                                        <div class="payment-selector-content">
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="40.00" checked>
                                                                <span class="payment-label">
                                                                        <span class="size">100<i class="coin"></i></span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="100.00">
                                                                <span class="payment-label">
                                                                        <span class="size">500<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +50<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="200.00">
                                                                <span class="payment-label">
                                                                        <span class="size">1000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +250<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="500.00">
                                                                <span class="payment-label">
                                                                        <span class="size">2000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +750<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="method-name">Оплата с лицевого счета МТС</div>
                                                    <div class="method-result">
                                                        <div class="method-result-name">Сумма платежа</div>
                                                        <div class="method-result-sum">
                                                            <span class="method-result-value">40.00</span>
                                                            <span class="method-result-currency">₽</span>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn">Оплатить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="tabs-item payment-type-content pay-megafon">
                                        <form class="form">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="payment-selector">

                                                        <div class="payment-selector-title"><span>Сколько кредитов положить на счет?</span></div>

                                                        <div class="payment-selector-content">
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="40.00" checked>
                                                                <span class="payment-label">
                                                                        <span class="size">100<i class="coin"></i></span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="100.00">
                                                                <span class="payment-label">
                                                                        <span class="size">500<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +50<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="200.00">
                                                                <span class="payment-label">
                                                                        <span class="size">1000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +250<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="500.00">
                                                                <span class="payment-label">
                                                                        <span class="size">2000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +750<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="method-name">Оплата с лицевого счета Мегафон</div>
                                                    <div class="method-result">
                                                        <div class="method-result-name">Сумма платежа</div>
                                                        <div class="method-result-sum">
                                                            <span class="method-result-value">40.00</span>
                                                            <span class="method-result-currency">₽</span>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn">Оплатить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="tabs-item payment-type-content pay-tele2">
                                        <form class="form">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="payment-selector">

                                                        <div class="payment-selector-title"><span>Сколько кредитов положить на счет?</span></div>

                                                        <div class="payment-selector-content">
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="40.00" checked>
                                                                <span class="payment-label">
                                                                        <span class="size">100<i class="coin"></i></span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="100.00">
                                                                <span class="payment-label">
                                                                        <span class="size">500<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +50<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="200.00">
                                                                <span class="payment-label">
                                                                        <span class="size">1000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +250<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="500.00">
                                                                <span class="payment-label">
                                                                        <span class="size">2000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +750<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="method-name">Оплата с лицевого счета TELE2</div>
                                                    <div class="method-result">
                                                        <div class="method-result-name">Сумма платежа</div>
                                                        <div class="method-result-sum">
                                                            <span class="method-result-value">40.00</span>
                                                            <span class="method-result-currency">₽</span>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn">Оплатить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="tabs-item payment-type-content pay-beeline">
                                        <form class="form">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="payment-selector">

                                                        <div class="payment-selector-title"><span>Сколько кредитов положить на счет?</span></div>

                                                        <div class="payment-selector-content">
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="40.00" checked>
                                                                <span class="payment-label">
                                                                        <span class="size">100<i class="coin"></i></span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="100.00">
                                                                <span class="payment-label">
                                                                        <span class="size">500<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +50<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="200.00">
                                                                <span class="payment-label">
                                                                        <span class="size">1000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +250<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="500.00">
                                                                <span class="payment-label">
                                                                        <span class="size">2000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +750<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="method-name">Оплата с лицевого счета Билайн</div>
                                                    <div class="method-result">
                                                        <div class="method-result-name">Сумма платежа</div>
                                                        <div class="method-result-sum">
                                                            <span class="method-result-value">40.00</span>
                                                            <span class="method-result-currency">₽</span>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn">Оплатить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>

                            </div>

                            <div class="tabs-item payment-type pay-coin">

                                <div class="tabs">

                                    <ul class="tabs-nav payment-type-nav payment-coin-elem">
                                        <li class="active"><a href="#" data-target=".pay-yandex">Yandex</a></li>
                                        <li><a href="#" data-target=".pay-webmoney">WebMoney</a></li>
                                        <li><a href="#" data-target=".pay-qiwi">Qiwi</a></li>
                                    </ul>

                                    <div class="tabs-item payment-type-content pay-yandex active">
                                        <form class="form">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="payment-selector">

                                                        <div class="payment-selector-title"><span>Сколько кредитов положить на счет?</span></div>

                                                        <div class="payment-selector-content">
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="40.00" checked>
                                                                <span class="payment-label">
                                                                        <span class="size">100<i class="coin"></i></span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="100.00">
                                                                <span class="payment-label">
                                                                        <span class="size">500<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +50<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="200.00">
                                                                <span class="payment-label">
                                                                        <span class="size">1000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +250<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="500.00">
                                                                <span class="payment-label">
                                                                        <span class="size">2000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +750<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="method-name">Оплата YandexДеньги</div>
                                                    <div class="method-result">
                                                        <div class="method-result-name">Сумма платежа</div>
                                                        <div class="method-result-sum">
                                                            <span class="method-result-value">40.00</span>
                                                            <span class="method-result-currency">₽</span>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn">Оплатить</button>
                                                    <div class="yellow-block">
                                                        или<br>
                                                        <a href="#">выписать счет</a><br>
                                                        Выписанный счёт действителен в течение 24-х часов.
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="tabs-item payment-type-content pay-webmoney">
                                        <form class="form">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="payment-selector">

                                                        <div class="payment-selector-title"><span>Сколько кредитов положить на счет?</span></div>

                                                        <div class="payment-selector-content">
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="40.00" checked>
                                                                <span class="payment-label">
                                                                        <span class="size">100<i class="coin"></i></span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="100.00">
                                                                <span class="payment-label">
                                                                        <span class="size">500<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +50<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="200.00">
                                                                <span class="payment-label">
                                                                        <span class="size">1000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +250<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="500.00">
                                                                <span class="payment-label">
                                                                        <span class="size">2000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +750<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="method-name">Оплата Web Money</div>
                                                    <div class="method-result">
                                                        <div class="method-result-name">Сумма платежа</div>
                                                        <div class="method-result-sum">
                                                            <span class="method-result-value">40.00</span>
                                                            <span class="method-result-currency">₽</span>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn">Оплатить</button>
                                                    <div class="yellow-block">
                                                        или<br>
                                                        <a href="#">выписать счет</a><br>
                                                        Выписанный счёт действителен в течение 24-х часов.
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="tabs-item payment-type-content pay-qiwi">
                                        <form class="form">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="payment-selector">

                                                        <div class="payment-selector-title"><span>Сколько кредитов положить на счет?</span></div>

                                                        <div class="payment-selector-content">
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="40.00" checked>
                                                                <span class="payment-label">
                                                                        <span class="size">100<i class="coin"></i></span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="100.00">
                                                                <span class="payment-label">
                                                                        <span class="size">500<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +50<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="200.00">
                                                                <span class="payment-label">
                                                                        <span class="size">1000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +250<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                            <label class="payment-selector-item">
                                                                <input type="radio" name="pay-variant" value="500.00">
                                                                <span class="payment-label">
                                                                        <span class="size">2000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +750<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="method-name">Оплата Qiwi</div>
                                                    <div class="method-result">
                                                        <div class="method-result-name">Сумма платежа</div>
                                                        <div class="method-result-sum">
                                                            <span class="method-result-value">40.00</span>
                                                            <span class="method-result-currency">₽</span>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn">Оплатить</button>
                                                    <div class="yellow-block">
                                                        или<br>
                                                        <a href="#">выписать счет</a><br>
                                                        Выписанный счёт действителен в течение 24-х часов.
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>

                            </div>

                            <div class="tabs-item payment-type pay-card">

                                <div class="payment-type-content">
                                    <form class="form">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="payment-selector">

                                                    <div class="payment-selector-title"><span>Сколько кредитов положить на счет?</span></div>

                                                    <div class="payment-selector-content">
                                                        <label class="payment-selector-item">
                                                            <input type="radio" name="pay-variant" value="40.00" checked>
                                                            <span class="payment-label">
                                                                        <span class="size">100<i class="coin"></i></span>
                                                                    </span>
                                                        </label>
                                                        <label class="payment-selector-item">
                                                            <input type="radio" name="pay-variant" value="100.00">
                                                            <span class="payment-label">
                                                                        <span class="size">500<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +50<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                        </label>
                                                        <label class="payment-selector-item">
                                                            <input type="radio" name="pay-variant" value="200.00">
                                                            <span class="payment-label">
                                                                        <span class="size">1000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +250<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                        </label>
                                                        <label class="payment-selector-item">
                                                            <input type="radio" name="pay-variant" value="500.00">
                                                            <span class="payment-label">
                                                                        <span class="size">2000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +750<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                        </label>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="method-name">Оплата кредитной картой</div>
                                                <div class="method-result">
                                                    <div class="method-result-name">Сумма платежа</div>
                                                    <div class="method-result-sum">
                                                        <span class="method-result-value">40.00</span>
                                                        <span class="method-result-currency">₽</span>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn">Оплатить</button>
                                                <div class="yellow-block">
                                                    Гарантированное время зачисления платежа — одна минута.
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>

                            <div class="tabs-item payment-type pay-paypal">

                                <div class="payment-type-content">
                                    <form class="form">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="payment-selector">

                                                    <div class="payment-selector-title"><span>Сколько кредитов положить на счет?</span></div>

                                                    <div class="payment-selector-content">
                                                        <label class="payment-selector-item">
                                                            <input type="radio" name="pay-variant" value="40.00" checked>
                                                            <span class="payment-label">
                                                                        <span class="size">100<i class="coin"></i></span>
                                                                    </span>
                                                        </label>
                                                        <label class="payment-selector-item">
                                                            <input type="radio" name="pay-variant" value="100.00">
                                                            <span class="payment-label">
                                                                        <span class="size">500<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +50<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                        </label>
                                                        <label class="payment-selector-item">
                                                            <input type="radio" name="pay-variant" value="200.00">
                                                            <span class="payment-label">
                                                                        <span class="size">1000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +250<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                        </label>
                                                        <label class="payment-selector-item">
                                                            <input type="radio" name="pay-variant" value="500.00">
                                                            <span class="payment-label">
                                                                        <span class="size">2000<i class="coin"></i></span>
                                                                        <span class="bonus-size">
                                                                            <span class="b">бонус</span> +750<i class="coin-m"><i></i></i>
                                                                        </span>
                                                                    </span>
                                                        </label>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="method-name">Оплата PayPal</div>
                                                <div class="method-result">
                                                    <div class="method-result-name">Сумма платежа</div>
                                                    <div class="method-result-sum">
                                                        <span class="method-result-value">40.00</span>
                                                        <span class="method-result-currency">₽</span>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn">Оплатить</button>
                                                <div class="yellow-block">
                                                    Гарантированное время зачисления платежа — одна минута.
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>

                        </div>

                        <div class="popular">
                            <h2 class="text_center"><a href="#">На что можно потратить деньги?</a></h2>
                            <ul class="clearfix">
                                <li>
                                    <a href="#" class="popular-image">
                                        <img class="img-responsive" src="img/increase_img01.png" alt="">
                                    </a>
                                    <a class="popular-title" href="#">Подарки</a>
                                </li>

                                <li>
                                    <a href="#"  class="popular-image">
                                        <img class="img-responsive" src="img/increase_img03.png" alt="">
                                    </a>
                                    <a class="popular-title" href="#">Первое место</a>
                                </li>

                                <li>
                                    <a href="#" class="popular-image">
                                        <img class="img-responsive" src="img/increase_img04.png" alt="">
                                    </a>
                                    <a class="popular-title" href="#">Встречи</a>
                                </li>
                                <li>
                                    <a href="#" class="popular-image">
                                        <img class="img-responsive" src="img/increase_img04.png" alt="">
                                    </a>
                                    <a class="popular-title" href="#">Фотолинейка</a>
                                </li>
                            </ul>
                        </div>

                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
<link rel="stylesheet" href="js/vendor/ionRangeSlider/css/ion.rangeSlider.css">