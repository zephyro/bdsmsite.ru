<header class="header-auth">
    <div class="container">
        <a href="/" class="header-logo">
            <img src="img/logo.png" class="img-responsive">
        </a>
        <div class="header-lng land">
            <a id="current" class="header-lng-current language" href="#"><span>Русский</span></a>
            <div class="lang_item" id="item">
                <ul class="pp_small" id="pp_language">
                    <li><a href="#"><span class="">English</span></a></li>
                    <li><a href="#"><span class="selected">Русский</span></a></li>
                </ul>
            </div>
        </div>
        <div class="header-info hidden-xs"><span>Еще не знакомы с нашим БДСМ сайтом?</span> <a href="./join">Попробуйте!</a></div>
    </div>
</header>