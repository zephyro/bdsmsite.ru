<div class="user-bar">
    <div class="container">
        <div class="user-bar-box">
            <ul class="user-bar-group user-bar-left">
                <li>
                    <a href="#" class="user-message" title="Сообщения">
                        <i class="fa fa-envelope"></i>
                        <span>Сообщения</span>
                        <sup>5</sup>
                    </a>
                </li>
                <li>
                    <a href="#" title="Сегодня вашу анкету никто не смотрел">
                        <i class="fa fa-eye"></i>
                        <sup>15</sup>
                    </a>
                </li>
                <li>
                    <a href="#" title="Поднаять в поиске"><i class="fa fa-long-arrow-up"></i></a>
                </li>
            </ul>
            <ul class="user-bar-group user-bar-right">
                <li>
                    <a href="#" title="На счету 150 кредитов" class="user-coin">
                        Счет <b>150</b> <span>монет</span> <i></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="Добавить фото">
                        <i class="fa fa-camera"></i>
                    </a>
                </li>
                <li class="hidden-xs">
                    <a class="user-vip" href="#" title="Супервозможности">
                        <i class="fa fa-star"></i>
                    </a>
                </li>
                <li class="btn-dropdown">
                    <a href="#"  title="Username"><i class="fa fa-user"></i> <span>Username</span> <sup>5</sup></a>
                    <ul class="dropdown-nav">
                        <li><a href="#"><i class="fa fa-user-o"></i> Анкета</a></li>
                        <li><a href="#"><i class="fa fa-wrench"></i> Настройки</a></li>
                        <li><a href="#"><i class="fa fa-volume-up"></i> Оповещения</a></li>
                        <li><a href="#"><i class="fa fa-support"></i> Поддержка</a></li>
                        <li><a href="#"><i class="fa fa-remove"></i> Выход</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>