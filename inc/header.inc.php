<header class="header">
    <div class="container">
        <a href="/" class="header-logo">
            <img src="img/logo.png" class="img-responsive">
        </a>

        <a href="#" class="nav-toggle">
            <span></span>
            <span></span>
            <span></span>
        </a>

        <ul class="header-nav">
            <li><a href="#"><span class="nav-people">Кто рядом</span></a></li>
            <li><a href="#"><span class="nav-encounters">Встречи</span></a></li>
            <li><a href="#"><span class="nav-msg">Сообщения</span></a></li>
            <li><a href="#"><span class="nav-ads">Объявления</span></a></li>
            <li class="nav-dropdown">
                <a href="#"><span class="nav-no-icon">Еще....</span></a>
                <ul class="header-dropnav">
                    <li><a href="#"><span>Форум</span></a></li>
                    <li><a href="#"><span>Даты</span></a></li>
                    <li><a href="#"><span>Места</span></a></li>
                    <li><a href="#"><span>Блоги</span></a></li>
                    <li><a href="#"><span>Статьи</span></a></li>
                    <li><a href="#"><span>Новости</span></a></li>
                    <li><a href="#"><span>3D-Город</span></a></li>
                </ul>
            </li>

        </ul>

    </div>
</header>