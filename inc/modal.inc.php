
<!-- Modal Payment -->
<div class="hide">
    <div class="payment" id="payment">
        <span class="modal-close btn-modal-close"></span>
        <div class="payment-heading"><span>Активируйте Супервозможности!</span> <a class="btn-modal hidden-xs" href="#super-info"><i class="fa fa-question"></i></a></div>

        <div class="payment-slider">
            <input type="text" id="payment-amount" name="payment-amount" value="" />
        </div>

        <div class="payment-method">
            <div class="payment-method-item">
                <a href="#" class="payment-method-nav active" data-target=".payment-recharge">
                    <span class="payment-icon payment-recharge"></span>
                    <span class="payment-name">Оплата кредитами</span>
                </a>
            </div>
            <div class="payment-method-item">
                <a href="#" class="payment-method-nav" data-target=".payment-mobile">
                    <span class="payment-icon payment-mobile"></span>
                    <span class="payment-name">Мобильный счет</span>
                </a>
            </div>
            <div class="payment-method-item">
                <a href="#" class="payment-method-nav" data-target=".payment-yandex">
                    <span class="payment-icon payment-yandex"></span>
                    <span class="payment-name">Яндекс Деньги</span>
                </a>
            </div>
            <div class="payment-method-item">
                <a href="#" class="payment-method-nav" data-target=".payment-card">
                    <span class="payment-icon payment-card"></span>
                    <span class="payment-name">Банковская карта</span>
                </a>
            </div>    <div class="payment-method-item">
                <a href="#" class="payment-method-nav" data-target=".payment-paypal">
                    <span class="payment-icon payment-paypal"></span>
                    <span class="payment-name">PayPal</span>
                </a>
            </div>    <div class="payment-method-item">
                <a href="#" class="payment-method-nav" data-target=".payment-qiwi">
                    <span class="payment-icon payment-qiwi"></span>
                    <span class="payment-name">QIWI Wallet</span>
                </a>
            </div>


        </div>

        <div class="payment-content payment-recharge payment-enable">
            <div class="payment-action">
                <div class="payment-action-left">
                    <button class="btn">Активировать</button>
                </div>
                <div class="payment-action-right">
                    <span class="total-price"><span class="total-price-value price-value-credit">200</span> кредитов</span>
                    <p class="payment-credits">Для активации не хватает <span class="credits-needed">200</span> кредитов</p>
                </div>
            </div>
        </div>

        <div class="payment-content payment-mobile">
            <div class="payment-content-info">
                <ul class="payment-operator">
                    <li><span data-value="mts">МТС</span></li>
                    <li><span data-value="megafon">Мегафон</span></li>
                    <li><span data-value="tele2">TELE2</span></li>
                    <li><span data-value="beeline">Билайн</span></li>
                </ul>
            </div>
            <div class="payment-action">
                <div class="payment-action-left">
                    <button class="btn">Активировать</button>
                </div>
                <div class="payment-action-right">
                    <span class="total-price"><span class="total-price-value price-value-pay">100</span> руб.</span>
                    <span>с НДС</span>
                    <span class="pay-creddits-profit">Выгода при оплате кредитами составляет <b>20</b> ₽</span>
                </div>
            </div>
        </div>

        <div class="payment-content payment-yandex">
            <div class="payment-action">
                <div class="payment-action-left">
                    <button class="btn">Активировать</button>
                </div>
                <div class="payment-action-right">
                    <span class="total-price"><span class="total-price-value price-value-pay">100</span> руб.</span>
                    <span>с НДС</span>
                    <span class="pay-creddits-profit">Выгода при оплате кредитами составляет <b>20</b> ₽</span>
                </div>
            </div>
        </div>

        <div class="payment-content payment-card">
            <div class="payment-action">
                <div class="payment-action-left">
                    <button class="btn">Активировать</button>
                </div>
                <div class="payment-action-right">
                    <span class="total-price"><span class="total-price-value price-value-pay">100</span> руб.</span>
                    <span>с НДС</span>
                    <span class="pay-creddits-profit">Выгода при оплате кредитами составляет <b>20</b> ₽</span>
                </div>
            </div>
        </div>

        <div class="payment-content payment-paypal">
            <div class="payment-action">
                <div class="payment-action-left">
                    <button class="btn">Активировать</button>
                </div>
                <div class="payment-action-right">
                    <span class="total-price"><span class="total-price-value price-value-pay">100</span> руб.</span>
                    <span>с НДС</span>
                    <span class="pay-creddits-profit">Выгода при оплате кредитами составляет <b>20</b> ₽</span>
                </div>
            </div>
        </div>

        <div class="payment-content payment-qiwi">
            <div class="payment-action">
                <div class="payment-action-left">
                    <button class="btn">Активировать</button>
                </div>
                <div class="payment-action-right">
                    <span class="total-price"><span class="total-price-value price-value-pay">100</span> руб.</span>
                    <span>с НДС</span>
                    <span class="pay-creddits-profit">Выгода при оплате кредитами составляет <b>20</b> ₽</span>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- -->

<!-- Modal Super Info -->
<div class="hide">
    <div class="super-modal" id="super-info">
        <div class="super-title">Что дают супервозможности</div>
        <ul class="super-list">
            <li>Вы сможете видеть, кто хотел бы с вами встретиться.</li>
            <li>Общайтесь с популярными пользователями</li>
            <li>Режим "Невидимка"</li>
            <li>Быстрая доставка сообщений</li>
            <li>Расширенный поиск</li>
        </ul>
        <div class="text-center">
            <a href="#" class="btn btn-modal-close">Понятно</a>
        </div>
    </div>
</div>
<!-- -->


<!-- Modal Popular -->
<div class="hide">
    <div class="payment" id="popular-up">
        <span class="modal-close btn-modal-close"></span>

        <div class="payment-heading">Что дают супервозможности</div>

        <ul class="popular-target">
            <li>
                <a href="#" data-value="">
                    <h4>Фотолинейка</h4>
                    <p>Ваша фотография появится в специальной бегущей ленте, вас смогут увидеть все пользователи вашего региона!</p>
                </a>
            </li>
            <li>
                <a href="#" data-value="">
                    <h4>Первое место</h4>
                    <p>Ваша анкета поднимется на первое место в результатах поиска, а, значит, вас легче будет найти!</p>
                </a>
            </li>
            <li>
                <a href="#" data-value="">
                    <h4>Чаще появляйтесь в Свиданиях</h4>
                    <p>Пусть ваша фотография появляется гораздо чаще в Свиданиях, так вы познакомитесь с большим количеством людей!</p>
                </a>
            </li>
        </ul>

        <div class="payment-slider">
            <input type="text" id="payment-amount-popular" name="payment-amount" value="" />
        </div>

        <div class="payment-method">
            <div class="payment-method-item">
                <a href="#" class="payment-method-nav active" data-target=".payment-recharge">
                    <span class="payment-icon payment-recharge"></span>
                    <span class="payment-name">Оплата кредитами</span>
                </a>
            </div>
            <div class="payment-method-item">
                <a href="#" class="payment-method-nav" data-target=".payment-mobile">
                    <span class="payment-icon payment-mobile"></span>
                    <span class="payment-name">Мобильный счет</span>
                </a>
            </div>
            <div class="payment-method-item">
                <a href="#" class="payment-method-nav" data-target=".payment-yandex">
                    <span class="payment-icon payment-yandex"></span>
                    <span class="payment-name">Яндекс Деньги</span>
                </a>
            </div>
            <div class="payment-method-item">
                <a href="#" class="payment-method-nav" data-target=".payment-card">
                    <span class="payment-icon payment-card"></span>
                    <span class="payment-name">Банковская карта</span>
                </a>
            </div>    <div class="payment-method-item">
                <a href="#" class="payment-method-nav" data-target=".payment-paypal">
                    <span class="payment-icon payment-paypal"></span>
                    <span class="payment-name">PayPal</span>
                </a>
            </div>    <div class="payment-method-item">
                <a href="#" class="payment-method-nav" data-target=".payment-qiwi">
                    <span class="payment-icon payment-qiwi"></span>
                    <span class="payment-name">QIWI Wallet</span>
                </a>
            </div>


        </div>

        <div class="payment-content payment-recharge payment-enable">
            <div class="payment-action">
                <div class="payment-action-left">
                    <button class="btn">Активировать</button>
                </div>
                <div class="payment-action-right">
                    <span class="total-price"><span class="total-price-value price-value-credit">200</span> кредитов</span>
                    <p class="payment-credits">Для активации не хватает <span class="credits-needed">200</span> кредитов</p>
                </div>
            </div>
        </div>

        <div class="payment-content payment-mobile">
            <div class="payment-content-info">
                <ul class="payment-operator">
                    <li><span data-value="mts">МТС</span></li>
                    <li><span data-value="megafon">Мегафон</span></li>
                    <li><span data-value="tele2">TELE2</span></li>
                    <li><span data-value="beeline">Билайн</span></li>
                </ul>
            </div>
            <div class="payment-action">
                <div class="payment-action-left">
                    <button class="btn">Активировать</button>
                </div>
                <div class="payment-action-right">
                    <span class="total-price"><span class="total-price-value price-value-pay">100</span> руб.</span>
                    <span>с НДС</span>
                    <span class="pay-creddits-profit">Выгода при оплате кредитами составляет <b>20</b> ₽</span>
                </div>
            </div>
        </div>

        <div class="payment-content payment-yandex">
            <div class="payment-action">
                <div class="payment-action-left">
                    <button class="btn">Активировать</button>
                </div>
                <div class="payment-action-right">
                    <span class="total-price"><span class="total-price-value price-value-pay">100</span> руб.</span>
                    <span>с НДС</span>
                    <span class="pay-creddits-profit">Выгода при оплате кредитами составляет <b>20</b> ₽</span>
                </div>
            </div>
        </div>

        <div class="payment-content payment-card">
            <div class="payment-action">
                <div class="payment-action-left">
                    <button class="btn">Активировать</button>
                </div>
                <div class="payment-action-right">
                    <span class="total-price"><span class="total-price-value price-value-pay">100</span> руб.</span>
                    <span>с НДС</span>
                    <span class="pay-creddits-profit">Выгода при оплате кредитами составляет <b>20</b> ₽</span>
                </div>
            </div>
        </div>

        <div class="payment-content payment-paypal">
            <div class="payment-action">
                <div class="payment-action-left">
                    <button class="btn">Активировать</button>
                </div>
                <div class="payment-action-right">
                    <span class="total-price"><span class="total-price-value price-value-pay">100</span> руб.</span>
                    <span>с НДС</span>
                    <span class="pay-creddits-profit">Выгода при оплате кредитами составляет <b>20</b> ₽</span>
                </div>
            </div>
        </div>

        <div class="payment-content payment-qiwi">
            <div class="payment-action">
                <div class="payment-action-left">
                    <button class="btn">Активировать</button>
                </div>
                <div class="payment-action-right">
                    <span class="total-price"><span class="total-price-value price-value-pay">100</span> руб.</span>
                    <span>с НДС</span>
                    <span class="pay-creddits-profit">Выгода при оплате кредитами составляет <b>20</b> ₽</span>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- -->