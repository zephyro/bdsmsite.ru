<footer class="footer">
    <div class="container">
        <div class="footer-row">
            <div class="footer-col footer-col-left">

                <ul class="footer-nav-primary flang-russian">
                    <li><a href="#">БДСМ мероприятия</a></li>
                    <li><a href="#">БДСМ заведения</a></li>
                    <li><a href="#">БДСМ объявления</a></li>
                </ul>

                <div class="footer-social">
                    <div class="footer-social-title">Наши БДСМ группы в соц сетях:</div>
                    <ul>
                        <li><a href="https://www.facebook.com/bdsmtheme"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://vk.com/club54805115"><i class="fa fa-vk"></i></a></li>
                        <li><a href="https://twitter.com/bdsmtheme"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://ok.ru/group50otte"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="https://instagram.com/siteruru/"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>

                <ul class="footer-nav-second clearfix">
                    <li><a href="http://forum.bdsmsite.ru/">Форум</a></li>
                    <li><a href="#">Новости</a></li>
                    <li><a href="#">Блоги</a></li>
                    <li><a href="#">Статьи</a></li>

                </ul>

            </div>
            <div class="footer-col footer-col-right">

                <ul class="footer-nav">
                    <li><a href="#">Поиск</a></li>
                    <li><a href="#">Войти</a></li>
                    <li><a href="#">Регистрация</a></li>
                    <li><a href="#">Правила пользования</a></li>
                </ul>

                <div class="footer-like">
                    <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script><div class="yashare-auto-init b-share_theme_counter" data-yasharel10n="ru" data-yasharetype="small" data-yasharequickservices="vkontakte,facebook,twitter,odnoklassniki,gplus" data-yasharetheme="counter" data-yasharelink="https://bdsmsite.ru/?set_language=russian"><span class="b-share b-share_type_small"><span class="b-share-btn__wrap"><a rel="nofollow" target="_blank" title="ВКонтакте" class="b-share__handle b-share__link b-share-btn__vkontakte" href="https://share.yandex.net/go.xml?service=vkontakte&amp;url=https%3A%2F%2Fbdsmsite.ru%2F%3Fset_language%3Drussian&amp;title=%D0%91%D0%94%D0%A1%D0%9C%D1%81%D0%B0%D0%B9%D1%82.%D1%80%D1%83%20-%20%D0%B1%D0%B5%D1%81%D0%BF%D0%BB%D0%B0%D1%82%D0%BD%D0%B0%D1%8F%20%D1%81%D0%B5%D1%82%D1%8C%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD%20%D0%B7%D0%BD%D0%B0%D0%BA%D0%BE%D0%BC%D1%81%D1%82%D0%B2%20%D0%B2%20BDSM%20%D1%82%D0%B5%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B5" data-service="vkontakte"><span class="b-share-icon b-share-icon_vkontakte"></span><span class="b-share-counter">0</span></a></span><span class="b-share-btn__wrap"><a rel="nofollow" target="_blank" title="Facebook" class="b-share__handle b-share__link b-share-btn__facebook" href="https://share.yandex.net/go.xml?service=facebook&amp;url=https%3A%2F%2Fbdsmsite.ru%2F%3Fset_language%3Drussian&amp;title=%D0%91%D0%94%D0%A1%D0%9C%D1%81%D0%B0%D0%B9%D1%82.%D1%80%D1%83%20-%20%D0%B1%D0%B5%D1%81%D0%BF%D0%BB%D0%B0%D1%82%D0%BD%D0%B0%D1%8F%20%D1%81%D0%B5%D1%82%D1%8C%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD%20%D0%B7%D0%BD%D0%B0%D0%BA%D0%BE%D0%BC%D1%81%D1%82%D0%B2%20%D0%B2%20BDSM%20%D1%82%D0%B5%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B5" data-service="facebook"><span class="b-share-icon b-share-icon_facebook"></span><span class="b-share-counter"></span></a></span><span class="b-share-btn__wrap"><a rel="nofollow" target="_blank" title="Twitter" class="b-share__handle b-share__link b-share-btn__twitter" href="https://share.yandex.net/go.xml?service=twitter&amp;url=https%3A%2F%2Fbdsmsite.ru%2F%3Fset_language%3Drussian&amp;title=%D0%91%D0%94%D0%A1%D0%9C%D1%81%D0%B0%D0%B9%D1%82.%D1%80%D1%83%20-%20%D0%B1%D0%B5%D1%81%D0%BF%D0%BB%D0%B0%D1%82%D0%BD%D0%B0%D1%8F%20%D1%81%D0%B5%D1%82%D1%8C%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD%20%D0%B7%D0%BD%D0%B0%D0%BA%D0%BE%D0%BC%D1%81%D1%82%D0%B2%20%D0%B2%20BDSM%20%D1%82%D0%B5%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B5" data-service="twitter"><span class="b-share-icon b-share-icon_twitter"></span></a></span><span class="b-share-btn__wrap"><a rel="nofollow" target="_blank" title="Одноклассники" class="b-share__handle b-share__link b-share-btn__odnoklassniki" href="https://share.yandex.net/go.xml?service=odnoklassniki&amp;url=https%3A%2F%2Fbdsmsite.ru%2F%3Fset_language%3Drussian&amp;title=%D0%91%D0%94%D0%A1%D0%9C%D1%81%D0%B0%D0%B9%D1%82.%D1%80%D1%83%20-%20%D0%B1%D0%B5%D1%81%D0%BF%D0%BB%D0%B0%D1%82%D0%BD%D0%B0%D1%8F%20%D1%81%D0%B5%D1%82%D1%8C%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD%20%D0%B7%D0%BD%D0%B0%D0%BA%D0%BE%D0%BC%D1%81%D1%82%D0%B2%20%D0%B2%20BDSM%20%D1%82%D0%B5%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B5" data-service="odnoklassniki"><span class="b-share-icon b-share-icon_odnoklassniki"></span><span class="b-share-counter">0</span></a></span><span class="b-share-btn__wrap"><a rel="nofollow" target="_blank" title="Google Plus" class="b-share__handle b-share__link b-share-btn__gplus" href="https://share.yandex.net/go.xml?service=gplus&amp;url=https%3A%2F%2Fbdsmsite.ru%2F%3Fset_language%3Drussian&amp;title=%D0%91%D0%94%D0%A1%D0%9C%D1%81%D0%B0%D0%B9%D1%82.%D1%80%D1%83%20-%20%D0%B1%D0%B5%D1%81%D0%BF%D0%BB%D0%B0%D1%82%D0%BD%D0%B0%D1%8F%20%D1%81%D0%B5%D1%82%D1%8C%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD%20%D0%B7%D0%BD%D0%B0%D0%BA%D0%BE%D0%BC%D1%81%D1%82%D0%B2%20%D0%B2%20BDSM%20%D1%82%D0%B5%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B5" data-service="gplus"><span class="b-share-icon b-share-icon_gplus"></span><span class="b-share-counter"></span></a></span><iframe style="display: none" src="//yastatic.net/share/ya-share-cnt.html?url=https%3A%2F%2Fbdsmsite.ru%2F%3Fset_language%3Drussian&amp;services=vkontakte,facebook,twitter,odnoklassniki,gplus"></iframe></span></div>
                </div>

                <div class="footer-copyright">BDSMsite 2017 ©</div>
                <a href="#" class="footer-logo">
                    <img src="img/footer_logo.png" class="img-responsive" alt="">
                </a>
            </div>
        </div>
    </div>
</footer>