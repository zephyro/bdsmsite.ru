
<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div class="top-line">
                        <ul class="breadcrumbs">
                            <li><a href="#">Главная</a></li>
                            <li>События</li>
                        </ul>

                        <div class="top-search">
                            <form class="form">
                                <input type="text" class="form-control form-control-small" name="search" placeholder="Поиск">
                                <button type="submit" class="btn btn-small"><i class="fa fa-search"></i> Поиск</button>
                            </form>
                        </div>

                    </div>

                    <div class="event-line">
                        <ul class="event-nav">
                            <li><a href="#">Все Вечеринки</a></li>
                            <li><a href="#">Семинары</a></li>
                            <li><a href="#">Мастер-классы</a></li>
                            <li><a href="#">Закрытые мероприятия</a></li>
                            <li><a href="#">Неформальные встречи</a></li>
                        </ul>

                        <div class="action-event-new">
                            <a href="#" class="btn btn-small"><i class="fa fa-pencil"></i> Создать</a>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-8 col-lg-8">

                            <h1>Создайте новое событие!</h1>

                            <div class="event-form">

                                <form class="form">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="event_name" placeholder="Название мероприятия">
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="event_info" placeholder="Описание события" rows="5"></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Страна</label>
                                                <select class="form-select">
                                                    <option value="">Россия</option>
                                                    <option value="">Германия</option>
                                                    <option value="">Италия</option>
                                                    <option value="">Турция</option>
                                                    <option value="">Армения</option>
                                                    <option value="">Эстония</option>
                                                    <option value="">Латвия</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Город</label>
                                                <select class="form-select">
                                                    <option value="">Москва</option>
                                                    <option value="">Волгоград</option>
                                                    <option value="">Тверь</option>
                                                    <option value="">Сочи</option>
                                                    <option value="">Саратов</option>
                                                    <option value="">Рязянь</option>
                                                    <option value="">Новосибирск</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="date" class="form-control" name="event_date" placeholder="Дата">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="time" class="form-control" name="event_time" placeholder="Время">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="event_address" placeholder="Адрес">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="event_place" placeholder="Название места">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="event_site" placeholder="Сайт">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="event_phone" placeholder="Телефон">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-md">Создать событие</button>
                                    </div>
                                </form>

                            </div>

                        </div>
                        <div class="col-md-4 col-lg-4">
                            <div class="side-box">
                                <div class="side-box-heading">Обсуждаемые</div>
                                <div class="side-box-inner">

                                    <div class="event-discuss">
                                        <div class="event-discuss-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" alt="" title="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="event-discuss-content">
                                            <a href="#" class="event-name">Очень интересное мероприятие</a>
                                            <ul class="event-meta">
                                                <li>Гости: 1</li>
                                                <li>Коммент.: 0</li>
                                                <li><a href="#">социальная сеть</a></li>
                                                <li><a href="#">24 Янв 2018</a> | 11:00</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="event-discuss">
                                        <div class="event-discuss-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" alt="" title="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="event-discuss-content">
                                            <a href="#" class="event-name">Сомое лучщее интересное мероприятие</a>
                                            <ul class="event-meta">
                                                <li>Гости: 105</li>
                                                <li>Коммент.: 20</li>
                                                <li><a href="#">социальная сеть</a></li>
                                                <li><a href="#">24 Янв 2018</a> | 11:00</li>
                                            </ul>
                                        </div>
                                    </div>



                                </div>
                                <div class="side-box-heading">Прошедшие</div>
                                <div class="side-box-inner">

                                    <div class="event-discuss">
                                        <div class="event-discuss-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" alt="" title="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="event-discuss-content">
                                            <a href="#" class="event-name">Очень интересное мероприятие</a>
                                            <ul class="event-meta">
                                                <li>Гости: 1</li>
                                                <li>Коммент.: 0</li>
                                                <li><a href="#">социальная сеть</a></li>
                                                <li><a href="#">24 Янв 2018</a> | 11:00</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="event-discuss">
                                        <div class="event-discuss-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" alt="" title="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="event-discuss-content">
                                            <a href="#" class="event-name">Сомое лучщее интересное мероприятие</a>
                                            <ul class="event-meta">
                                                <li>Гости: 105</li>
                                                <li>Коммент.: 20</li>
                                                <li><a href="#">социальная сеть</a></li>
                                                <li><a href="#">24 Янв 2018</a> | 11:00</li>
                                            </ul>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
