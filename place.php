
<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div class="top-line">
                        <ul class="breadcrumbs">
                            <li><a href="#">Главная</a></li>
                            <li>События</li>
                        </ul>

                        <div class="top-search">
                            <form class="form">
                                <input type="text" class="form-control form-control-small" name="search" placeholder="Поиск">
                                <button type="submit" class="btn btn-small"><i class="fa fa-search"></i> Поиск</button>
                            </form>
                        </div>

                    </div>

                    <div class="event-line">
                        <ul class="event-nav">
                            <li><a href="#">Все Вечеринки</a></li>
                            <li><a href="#">Семинары</a></li>
                            <li><a href="#">Мастер-классы</a></li>
                            <li><a href="#">Закрытые мероприятия</a></li>
                            <li><a href="#">Неформальные встречи</a></li>
                        </ul>

                        <div class="action-event-new">
                            <a href="#" class="btn btn-small"><i class="fa fa-pencil"></i> Создать</a>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-8 col-lg-8">

                            <div class="event-title">
                                <h1>Клуб свободных отношений "Красавица и Чудовище"Рейтинг:</h1>
                                <div class="rate">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>

                            <div class="event">
                                <div class="event-photo">
                                    <a href="#" data-fancybox="gallery">
                                        <img src="images/article_01.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <div class="event-content">
                                    <ul class="event-info">
                                        <li><strong>Категория:</strong> <a href="#">Неформальные встречи</a></li>
                                        <li><strong>Адрес:</strong> <a href="#">Россия, Санкт-Петербург, Невский проспект, 5</a></li>
                                        <li><strong>Телефон:</strong> <span>+7 (812) 333-33-33</span></li>
                                        <li><strong>Гости:</strong> 5, <strong>Коментарии:</strong> 0</li>
                                    </ul>
                                </div>
                            </div>


                            <ul class="comments">
                                <li>
                                    <div class="article-comments-author">
                                        <div class="author-image">
                                            <img src="images/author_01.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="author-meta">
                                            <a href="#">User Name</a>
                                            <span>2017-02-23 15:39</span>
                                        </div>
                                    </div>
                                    <div class="article-comments-content">
                                        <p>Забавные наблюдения) Затрудняюсь сказать, на какую породу похожа моя девушка) Она любит выглядеть стильно. Ошейник предпочитает металлический, потому что он подходит под цвет любой одежды и прекрасно смотрится без нее. Цепь мы не используем, она тяжелая, заменяем поводком./</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="article-comments-author">
                                        <div class="author-image">
                                            <img src="images/author_01.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="author-meta">
                                            <a href="#">User Name</a>
                                            <span>2017-02-23 15:39</span>
                                        </div>
                                    </div>
                                    <div class="article-comments-content">
                                        <p>Забавные наблюдения) Затрудняюсь сказать, на какую породу похожа моя девушка) Она любит выглядеть стильно. Ошейник предпочитает металлический, потому что он подходит под цвет любой одежды и прекрасно смотрится без нее. Цепь мы не используем, она тяжелая, заменяем поводком./</p>
                                    </div>

                                    <ul>
                                        <li>
                                            <div class="article-comments-author">
                                                <div class="author-image">
                                                    <img src="images/author_01.jpg" class="img-responsive" alt="">
                                                </div>
                                                <div class="author-meta">
                                                    <a href="#">User Name</a>
                                                    <span>2017-02-23 15:39</span>
                                                </div>
                                            </div>
                                            <div class="article-comments-content">
                                                <p>Забавные наблюдения) Затрудняюсь сказать, на какую породу похожа моя девушка) Она любит выглядеть стильно. Ошейник предпочитает металлический, потому что он подходит под цвет любой одежды и прекрасно смотрится без нее. Цепь мы не используем, она тяжелая, заменяем поводком./</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="article-comments-author">
                                                <div class="author-image">
                                                    <img src="images/author_01.jpg" class="img-responsive" alt="">
                                                </div>
                                                <div class="author-meta">
                                                    <a href="#">User Name</a>
                                                    <span>2017-02-23 15:39</span>
                                                </div>
                                            </div>
                                            <div class="article-comments-content">
                                                <p>Забавные наблюдения) Затрудняюсь сказать, на какую породу похожа моя девушка) Она любит выглядеть стильно. Ошейник предпочитает металлический, потому что он подходит под цвет любой одежды и прекрасно смотрится без нее. Цепь мы не используем, она тяжелая, заменяем поводком./</p>
                                            </div>
                                        </li>
                                    </ul>

                                </li>
                                <li>
                                    <div class="article-comments-author">
                                        <div class="author-image">
                                            <img src="images/author_01.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="author-meta">
                                            <a href="#">User Name</a>
                                            <span>2017-02-23 15:39</span>
                                        </div>
                                    </div>
                                    <div class="article-comments-content">
                                        <p>Забавные наблюдения) Затрудняюсь сказать, на какую породу похожа моя девушка) Она любит выглядеть стильно. Ошейник предпочитает металлический, потому что он подходит под цвет любой одежды и прекрасно смотрится без нее. Цепь мы не используем, она тяжелая, заменяем поводком./</p>
                                    </div>
                                </li>
                            </ul>

                            <div class="omments-new">
                                <form class="form">
                                    <div class="form-group">
                                        <textarea class="form-control" name="message" placeholder="Ваш комментарий" rows="6"></textarea>
                                    </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-md">Отправить</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                        <div class="col-md-4 col-lg-4">
                            <div class="side-box">
                                <div class="side-box-heading">Отзыв дня</div>
                                <div class="side-box-inner">

                                    <div class="place place-sm">
                                        <div class="place-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" class="img-responsive" alt="">
                                            </a>
                                        </div>
                                        <div class="place-content">
                                            <a href="#" class="place-name">Клуб свободных отношений "Красавица и Чудовище"</a>
                                            <div class="place-info">Санкт-Петербург, 1 отзыв</div>
                                            <div class="rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="place-review-author">От <a href="#"">Karlson</a></div>
                                        </div>
                                    </div>




                                </div>
                                <div class="side-box-heading">Популярные</div>
                                <div class="side-box-inner">

                                    <div class="place place-sm">
                                        <div class="place-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" class="img-responsive" alt="">
                                            </a>
                                        </div>
                                        <div class="place-content">
                                            <a href="#" class="place-name">Клуб свободных отношений "Красавица и Чудовище"</a>
                                            <div class="place-info">Санкт-Петербург, 1 отзыв</div>
                                            <div class="rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="place-review-author">От <a href="#"">Karlson</a></div>
                                        </div>
                                    </div>

                                    <div class="place place-sm">
                                        <div class="place-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" class="img-responsive" alt="">
                                            </a>
                                        </div>
                                        <div class="place-content">
                                            <a href="#" class="place-name">Клуб свободных отношений "Красавица и Чудовище"</a>
                                            <div class="place-info">Санкт-Петербург, 1 отзыв</div>
                                            <div class="rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="place-review-author">От <a href="#"">Karlson</a></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
