<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div class="row">

                        <div class="col-right">

                            <div class="find-block">

                                <div class="search-header">
                                    <span class="search-title">Поиск по</span>
                                    <a href="#" class="btn-border current">по анкетам</a>
                                    <a href="#" class="btn-border">по обьявлению</a>
                                </div>

                                <ul class="search-content">
                                    <li>
                                        <label class="main-label">Я ищу</label>
                                        <div class="main-select">
                                            <div class="main-select-selected">
                                                <span class="main-select-value">девушку</span>
                                            </div>
                                            <div class="main-select-dropdown">
                                                <ul>
                                                    <li>
                                                        <label class="main-select-label">
                                                            <input type="radio" name="looking" value="девушку" checked>
                                                            <span>девушку</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="main-select-label">
                                                            <input type="radio" name="looking" value="парня">
                                                            <span>парня</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="main-select-label">
                                                            <input type="radio" name="looking" value="пару">
                                                            <span>пару</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <label class="main-label">Роль</label>
                                        <div class="main-select">
                                            <div class="main-select-selected">
                                                <span class="main-select-value">верх</span>
                                            </div>
                                            <div class="main-select-dropdown">
                                                <ul>
                                                    <li>
                                                        <label class="main-select-label">
                                                            <input type="radio" name="role" value="верх" checked>
                                                            <span>верх</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="main-select-label">
                                                            <input type="radio" name="role" value="низ">
                                                            <span>низ</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="main-select-label">
                                                            <input type="radio" name="role" value="свитч">
                                                            <span>свитч</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <label class="main-label">Возраст</label>
                                        <div class="main-select">
                                            <div class="main-select-selected">
                                                От <span class="slider-value-from">20</span> до <span class="slider-value-to">35</span> лет
                                            </div>
                                            <div class="main-select-dropdown">
                                                <div class="main-select-slider">
                                                    <div class="select-slider-wrap">
                                                        <input type="text" id="range" value="" name="range" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <label class="main-label">Откуда</label>
                                        <button class="select-button btn-city-select" value="Москва">Москва</button>
                                    </li>
                                    <li>
                                        <button type="submit" class="btn">Найти</button>
                                    </li>
                                </ul>

                            </div>

                            <div class="search-result">
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_02.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_02.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_02.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_02.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_02.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                                <div class="search-result-item">
                                    <a href="#">
                                        <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                        <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                    </a>
                                </div>
                            </div>

                            <ul class="pagination">
                                <li><span>1</span></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">6</a></li>
                                <li><a href="#">7</a></li>
                            </ul>

                        </div>

                        <div class="col-left">

                            <div class="side-box">
                                <div class="side-box-inner">

                                    <div class="text-center">
                                        <a href="#" class="btn-text">Мои объявления</a>
                                    </div>

                                    <div class="text-center">
                                        <a class="btn btn-md" href="#">Подать обьявление</a>
                                    </div>
                                    <br/>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <!-- Sliders -->

        <script language="javascript" type="text/javascript">

            $( document ).ready(function() {

                $(function () {

                    var $range = $("#range")

                    $range.ionRangeSlider({
                        hide_min_max: true,
                        keyboard: true,
                        min: 18,
                        max: 80,
                        from: 20,
                        to: 35,
                        type: 'double',
                        step: 1,
                        prefix: "",
                        grid: false
                    });

                    $range.on("change", function () {


                        var $this = $(this),
                            from = $this.data("from"),
                            to = $this.data("to");

                        console.log(from + " - " + to);

                        $(this).closest('.main-select').find('.slider-value-from').text(from);
                        $(this).closest('.main-select').find('.slider-value-to').text(to);
                    });

                });

            });

        </script>

        <!-- END Sliders -->

    </body>

</html>
<link rel="stylesheet" href="js/vendor/ionRangeSlider/css/ion.rangeSlider.css">