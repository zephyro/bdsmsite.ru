<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div class="row">

                        <div class="col-right">

                            <div class="box-border">
                                <div class="box-border-header no-bg"><i class="fa fa-cog"></i> Ваши настройки</div>

                                <div class="setting-box">
                                    <h3>Настройка профиля</h3>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Язык интерфейса</label>
                                                <select class="form-select">
                                                    <option value="">Русский</option>
                                                    <option value="">Английский</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Кто может просматривать ваш профиль</label>
                                                <select class="form-select">
                                                    <option value="">Все</option>
                                                    <option value="">Зарегистрированные</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Сообщения на стене</label>
                                                <select class="form-select">
                                                    <option value="">Все</option>
                                                    <option value="">Только друзья</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Часовой пояс</label>
                                                <select class="form-select">
                                                    <option value="">Выберете город</option>
                                                    <option value="">Москва</option>
                                                    <option value="">Новосибирск</option>
                                                    <option value="">Магадан</option>
                                                    <option value="">Минск</option>
                                                    <option value="">Владивосток</option>
                                                    <option value="">Лондон</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                                <div class="setting-box">
                                    <h3>Оповещения по почте</h3>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Сообщения о новых лайках и комментариях</label>
                                                <select class="form-select">
                                                    <option value="">Включить</option>
                                                    <option value="">Оключить</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Посещение профиля</label>
                                                <select class="form-select">
                                                    <option value="">Включить</option>
                                                    <option value="">Оключить</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Подарки</label>
                                                <select class="form-select">
                                                    <option value="">Включить</option>
                                                    <option value="">Оключить</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Оценки фотографий</label>
                                                <select class="form-select">
                                                    <option value="">Включить</option>
                                                    <option value="">Оключить</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Оповещение о проявлении интереса</label>
                                                <select class="form-select">
                                                    <option value="">Включить</option>
                                                    <option value="">Оключить</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Cообщение с новыми людьми</label>
                                                <select class="form-select">
                                                    <option value="">Включить</option>
                                                    <option value="">Оключить</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Рассылка новости сервиса</label>
                                                <select class="form-select">
                                                    <option value="">Включить</option>
                                                    <option value="">Оключить</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="setting-box">
                                    <h3>Режим "Невидимка"</h3>
                                    <div class="form-group">
                                        <a href="#" class="btn btn-brown">Активировать супервозможности</a>
                                    </div>
                                </div>
                                <div class="setting-box">
                                    <h3>Изменить пароль</h3>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Текущий пароль</label>
                                                <input type="password" class="form-control" name="" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Новый пароль</label>
                                                <input type="password" class="form-control" name="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Повторите пароль</label>
                                                <input type="password" class="form-control" name="" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="setting-box">
                                    <h3>Изменить почту</h3>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Старый E-mail</label>
                                                <input type="password" class="form-control" name="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="label-control">Новый E-mail</label>
                                                <input type="password" class="form-control" name="" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-md"><i class="fa fa-check-square-o"></i> Готово</button>

                            </div>



                        </div>

                        <div class="col-left">

                            <div class="row">
                                <div class="col-sm-6 col-md-12">
                                    <div class="side-box">
                                        <div class="side-super">
                                            <ul class="super-row">
                                                <li><a href="#payment" class="btn btn-modal-payment"><i class="fa fa-star"></i> Супервозможности</a></li>
                                                <li class="hidden-md"><a href="#" class="btn btn-brown btn-symbol"><i class="fa fa-question"></i></a></li>
                                            </ul>
                                            <div class="side-stats">
                                                <a href="#">
                                                    <span>Заходили к вам</span>
                                                    <b>3</b>
                                                </a>
                                                <a href="#">
                                                    <span>Чат</span>
                                                    <b>0</b>
                                                </a>
                                                <a href="#">
                                                    <span>Оценили ваши фото</span>
                                                    <b>2</b>
                                                </a>
                                                <a href="#">
                                                    <span>Друзья</span>
                                                    <b>10</b>
                                                </a>
                                                <a href="#">
                                                    <span>Вы нравитесь</span>
                                                    <b>12</b>
                                                </a>
                                                <a href="#">
                                                    <span>Взаимная симпатия</span>
                                                    <b>2</b>
                                                </a>
                                                <a href="#">
                                                    <span>Видят приватные фото</span>
                                                    <b>1</b>
                                                </a>
                                                <a href="#">
                                                    <span>Заблокированы</span>
                                                    <b>0</b>
                                                </a>
                                                <a href="#">
                                                    <span>Подмигнули</span>
                                                    <b>10</b>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
<link rel="stylesheet" href="js/vendor/ionRangeSlider/css/ion.rangeSlider.css">