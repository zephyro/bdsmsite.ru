<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div class="row">

                        <div class="col-right">

                            <div class="box-border">
                                <div class="box-border-header no-bg"><i class="fa fa-user-o"></i> Посетители вашего профиля</div>

                                <div class="row">

                                    <div class="col-sm-6">

                                        <div class="visitor">
                                            <div class="visitor-image">
                                                <a href="#">
                                                    <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                                </a>
                                            </div>
                                            <div class="visitor-text">
                                                <div class="visitor-info"><a href="#">Anastasia Orlova,</a><span>26,</span><span>Красноборск</span></div>
                                                <div class="visitor-date">7 дней назад, зашла</div>
                                            </div>
                                            <ul class="visitor-action">
                                                <li><a href="#" title="Чат"><i class="fa fa-comments-o"></i><span>Чат</span></a></li>
                                                <li><a href="#" title="Подарок"><i class="fa fa-shopping-bag"></i><span>Подарок</span></a></li>
                                                <li><a href="#" title="Блокировать"><i class="fa fa-lock"></i><span>Блок.</span></a></li>
                                                <li><a href="#" title="Удалить"><i class="fa fa-close"></i><span>Удалить</span></a></li>
                                            </ul>
                                        </div>

                                    </div>

                                    <div class="col-sm-6">

                                        <div class="visitor">
                                            <div class="visitor-image">
                                                <a href="#">
                                                    <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                                </a>
                                            </div>
                                            <div class="visitor-text">
                                                <div class="visitor-info"><a href="#">Anastasia Orlova,</a><span>26,</span><span>Красноборск</span></div>
                                                <div class="visitor-date">7 дней назад, зашла</div>
                                            </div>
                                            <ul class="visitor-action">
                                                <li><a href="#" title="Чат"><i class="fa fa-comments-o"></i><span>Чат</span></a></li>
                                                <li><a href="#" title="Подарок"><i class="fa fa-shopping-bag"></i><span>Подарок</span></a></li>
                                                <li><a href="#" title="Блокировать"><i class="fa fa-lock"></i><span>Блок.</span></a></li>
                                                <li><a href="#" title="Удалить"><i class="fa fa-close"></i><span>Удалить</span></a></li>
                                            </ul>
                                        </div>

                                    </div>

                                    <div class="col-sm-6">

                                        <div class="visitor">
                                            <div class="visitor-image">
                                                <a href="#">
                                                    <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                                </a>
                                            </div>
                                            <div class="visitor-text">
                                                <div class="visitor-info"><a href="#">Anastasia Orlova,</a><span>26,</span><span>Красноборск</span></div>
                                                <div class="visitor-date">7 дней назад, зашла</div>
                                            </div>
                                            <ul class="visitor-action">
                                                <li><a href="#" title="Чат"><i class="fa fa-comments-o"></i><span>Чат</span></a></li>
                                                <li><a href="#" title="Подарок"><i class="fa fa-shopping-bag"></i><span>Подарок</span></a></li>
                                                <li><a href="#" title="Блокировать"><i class="fa fa-lock"></i><span>Блок.</span></a></li>
                                                <li><a href="#" title="Удалить"><i class="fa fa-close"></i><span>Удалить</span></a></li>
                                            </ul>
                                        </div>

                                    </div>

                                    <div class="col-sm-6">

                                        <div class="visitor">
                                            <div class="visitor-image">
                                                <a href="#">
                                                    <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                                </a>
                                            </div>
                                            <div class="visitor-text">
                                                <div class="visitor-info"><a href="#">Anastasia Orlova,</a><span>26,</span><span>Красноборск</span></div>
                                                <div class="visitor-date">7 дней назад, зашла</div>
                                            </div>
                                            <ul class="visitor-action">
                                                <li><a href="#" title="Чат"><i class="fa fa-comments-o"></i><span>Чат</span></a></li>
                                                <li><a href="#" title="Подарок"><i class="fa fa-shopping-bag"></i><span>Подарок</span></a></li>
                                                <li><a href="#" title="Блокировать"><i class="fa fa-lock"></i><span>Блок.</span></a></li>
                                                <li><a href="#" title="Удалить"><i class="fa fa-close"></i><span>Удалить</span></a></li>
                                            </ul>
                                        </div>

                                    </div>

                                </div>

                            </div>



                        </div>

                        <div class="col-left">

                            <div class="row">
                                <div class="col-sm-6 col-md-12">
                                    <div class="side-box">
                                        <div class="side-super">
                                            <ul class="super-row">
                                                <li><a href="#payment" class="btn btn-modal-payment"><i class="fa fa-star"></i> Супервозможности</a></li>
                                                <li class="hidden-md"><a href="#" class="btn btn-brown btn-symbol"><i class="fa fa-question"></i></a></li>
                                            </ul>
                                            <div class="side-stats">
                                                <a href="#">
                                                    <span>Заходили к вам</span>
                                                    <b>3</b>
                                                </a>
                                                <a href="#">
                                                    <span>Чат</span>
                                                    <b>0</b>
                                                </a>
                                                <a href="#">
                                                    <span>Оценили ваши фото</span>
                                                    <b>2</b>
                                                </a>
                                                <a href="#">
                                                    <span>Друзья</span>
                                                    <b>10</b>
                                                </a>
                                                <a href="#">
                                                    <span>Вы нравитесь</span>
                                                    <b>12</b>
                                                </a>
                                                <a href="#">
                                                    <span>Взаимная симпатия</span>
                                                    <b>2</b>
                                                </a>
                                                <a href="#">
                                                    <span>Видят приватные фото</span>
                                                    <b>1</b>
                                                </a>
                                                <a href="#">
                                                    <span>Заблокированы</span>
                                                    <b>0</b>
                                                </a>
                                                <a href="#">
                                                    <span>Подмигнули</span>
                                                    <b>10</b>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
<link rel="stylesheet" href="js/vendor/ionRangeSlider/css/ion.rangeSlider.css">