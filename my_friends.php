<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div class="row">

                        <div class="col-right">

                            <div class="box-border">
                                <div class="box-border-header no-bg"><i class="fa fa-picture-o"></i> Видят ваши приватные фото</div>

                                <div class="no-rate">
                                    <i class="fa fa-user-circle"></i>
                                    <span>Тут пока никого нет</span>
                                </div>

                            </div>



                        </div>

                        <div class="col-left">

                            <div class="row">
                                <div class="col-sm-6 col-md-12">
                                    <div class="side-box">
                                        <div class="side-super">
                                            <ul class="super-row">
                                                <li><a href="#payment" class="btn btn-modal-payment"><i class="fa fa-star"></i> Супервозможности</a></li>
                                                <li class="hidden-md"><a href="#" class="btn btn-brown btn-symbol"><i class="fa fa-question"></i></a></li>
                                            </ul>
                                            <div class="side-stats">
                                                <a href="#">
                                                    <span>Заходили к вам</span>
                                                    <b>3</b>
                                                </a>
                                                <a href="#">
                                                    <span>Чат</span>
                                                    <b>0</b>
                                                </a>
                                                <a href="#">
                                                    <span>Оценили ваши фото</span>
                                                    <b>2</b>
                                                </a>
                                                <a href="#">
                                                    <span>Друзья</span>
                                                    <b>10</b>
                                                </a>
                                                <a href="#">
                                                    <span>Вы нравитесь</span>
                                                    <b>12</b>
                                                </a>
                                                <a href="#">
                                                    <span>Взаимная симпатия</span>
                                                    <b>2</b>
                                                </a>
                                                <a href="#">
                                                    <span>Видят приватные фото</span>
                                                    <b>1</b>
                                                </a>
                                                <a href="#">
                                                    <span>Заблокированы</span>
                                                    <b>0</b>
                                                </a>
                                                <a href="#">
                                                    <span>Подмигнули</span>
                                                    <b>10</b>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
<link rel="stylesheet" href="js/vendor/ionRangeSlider/css/ion.rangeSlider.css">