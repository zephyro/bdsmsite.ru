<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.auth.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div class="row">

                        <div class="col-md-right">

                            <div class="find-block">

                                <ul class="search-content">
                                    <li>
                                        <label class="main-label">Я ищу</label>
                                        <div class="main-select">
                                            <div class="main-select-selected">
                                                <span class="main-select-value">девушку</span>
                                            </div>
                                            <div class="main-select-dropdown">
                                                <ul>
                                                    <li>
                                                        <label class="main-select-label">
                                                            <input type="radio" name="looking" value="девушку" checked>
                                                            <span>девушку</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="main-select-label">
                                                            <input type="radio" name="looking" value="парня">
                                                            <span>парня</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="main-select-label">
                                                            <input type="radio" name="looking" value="пару">
                                                            <span>пару</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <label class="main-label">Роль</label>
                                        <div class="main-select">
                                            <div class="main-select-selected">
                                                <span class="main-select-value">верх</span>
                                            </div>
                                            <div class="main-select-dropdown">
                                                <ul>
                                                    <li>
                                                        <label class="main-select-label">
                                                            <input type="radio" name="role" value="верх" checked>
                                                            <span>верх</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="main-select-label">
                                                            <input type="radio" name="role" value="низ">
                                                            <span>низ</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="main-select-label">
                                                            <input type="radio" name="role" value="свитч">
                                                            <span>свитч</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <label class="main-label">Возраст</label>
                                        <div class="main-select">
                                            <div class="main-select-selected">
                                                От <span class="slider-value-from">20</span> до <span class="slider-value-to">35</span> лет
                                            </div>
                                            <div class="main-select-dropdown">
                                                <div class="main-select-slider">
                                                    <div class="select-slider-wrap">
                                                        <input type="text" id="range" value="" name="range" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <label class="main-label">Откуда</label>
                                        <button class="select-button btn-city-select" value="Москва">Москва</button>
                                    </li>
                                    <li>
                                        <button type="submit" class="btn">Найти</button>
                                    </li>
                                </ul>

                            </div>


                            <div class="people-gallery hidden-xs hidden-sm">
                                <div class="people-gallery-wrap">
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_03.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">2 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_02.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_05.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">4 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_04.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">1 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>

                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_03.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">2 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_02.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_04.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">1 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_05.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">4 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_03.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">2 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_02.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_05.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">4 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_04.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">1 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="people-block">
                                <div class="people-block-title">Друзья в городе <a href="#" class="btn-city-select">Москва</a></div>

                                <div class="people-slider">

                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_02.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_03.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_04.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_05.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_03.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-left">

                            <aside class="sidebar">

                                <div class="sidebar-auth">

                                    <div class="auth-login">
                                        <form class="form">
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-icon" name="login" placeholder="Почта/имя:">
                                                <span class="form-icon"><i class="fa fa-user"></i></span>
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control form-control-icon" name="password" placeholder="пароль">
                                                <span class="form-icon"><i class="fa fa-lock"></i></span>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-row">
                                                    <div class="form-column">
                                                        <label class="input-checkbox">
                                                            <input type="checkbox" value="">
                                                            <span class="input-label">Запомнить меня</span>
                                                        </label>
                                                    </div>
                                                    <div class="form-column">
                                                        <a href="#" class="forgot-password"><span>Забыли пароль?</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn">Войти</button>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="auth-social">
                                        <div class="auth-social-title"><span>Войти через</span></div>
                                        <div class="auth-social-list">
                                            <a href="#" title="Войти через Facebook"><i class="fa fa-facebook"></i></a>
                                            <a href="#" title="Войти через Одноклассники"><i class="fa fa-odnoklassniki"></i></a>
                                            <a href="#" title="Войти через Вконтакте"><i class="fa fa-vk"></i></a>
                                            <a href="#" title="Войти через Google"><i class="fa fa-google"></i></a>
                                            <a href="#" title="Войти через Yandex"><i class="yandex">Я</i></a>
                                        </div>
                                    </div>

                                    <div class="auth-new-user">
                                        <a href="#" class="btn btn-brown">Регистрация</a>
                                    </div>

                                </div>

                            </aside>

                        </div>

                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


        <!-- Sliders -->

        <script language="javascript" type="text/javascript">

            $( document ).ready(function() {

                $(function () {

                    var $range = $("#range")

                    $range.ionRangeSlider({
                        hide_min_max: true,
                        keyboard: true,
                        min: 18,
                        max: 80,
                        from: 20,
                        to: 35,
                        type: 'double',
                        step: 1,
                        prefix: "",
                        grid: false
                    });

                    $range.on("change", function () {


                        var $this = $(this),
                            from = $this.data("from"),
                            to = $this.data("to");

                        console.log(from + " - " + to);

                        $(this).closest('.main-select').find('.slider-value-from').text(from);
                        $(this).closest('.main-select').find('.slider-value-to').text(to);
                    });

                });

            });

        </script>

        <!-- END Sliders -->

    </body>

</html>
