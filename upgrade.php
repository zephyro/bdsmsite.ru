
<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->


            <div class="main">
                <div class="container">

                    <div title="row">

                        <div class="col-md-4 col-lg-4">

                            <a href="#" class="go-back"><i class="fa fa-backward" aria-hidden="true"></i> <span>Назад</span></a>

                        </div>

                        <div class="col-md-8 col-lg-8">

                            <h1>Активируйте Супервозможности!</h1>

                            <ul class="opportunity">
                                <li>
                                    <strong>Встречи - выясните, кто хочет с вами встретиться</strong>
                                    <span>Вы сможете видеть, кто хотел бы с вами встретиться..</span>
                                </li>
                                <li>
                                    <strong>Общайтесь с популярными пользователями</strong>
                                    <span>Получайте эксклюзивный доступ к самым популярным людям!</span>
                                </li>
                                <li>
                                    <strong>Режим "Невидимка"</strong>
                                    <span>Заходите к другим людям в профиль так, чтобы они никогда не видели, что вы у них были!</span>
                                </li>
                                <li>
                                    <strong>Быстрая доставка сообщений</strong>
                                    <span>Все ваши сообщения будут читаться первыми.</span>
                                </li>
                                <li>
                                    <strong>Расширенный поиск</strong>
                                    <span>Ищите свою половинку по более точным параметрам и никогда не ошибетесь!</span>
                                </li>
                            </ul>

                            <div class="text-center">
                                <a href="#payment" class="btn btn-md btn-modal-payment"><i class="fa fa-star"></i> Супервозможности</a>
                            </div>

                        </div>

                    </div>


                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
