<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div class="row">

                        <div class="col-right">

                            <div class="row profile-main">

                                <div class="col-sm-5 col-md-5 col-lg-4">

                                    <div class="profile-photo-block">

                                        <div class="profile-photo">
                                            <div class="no-photo">
                                                <div class="no-photo-text">Нет фото :(</div>
                                                <div class="no-photo-icon">
                                                    <img src="img/no-photo.png" alt="">
                                                </div>
                                                <a href="#" class="btn">Загрузить</a>
                                            </div>
                                        </div>
                                        <a href="#" class="profile-photo-album">У меня 2 альбома</a>

                                    </div>
                                </div>

                                <div class="col-sm-7 col-md-7 col-lg-8">

                                    <div class="profile-info">
                                        <h1 class="profile-username">Olaf Adams</h1>
                                        <div class="profile-misc"><span>30 лет</span>, <a href="#">Рак</a> <a href="#">Россия, Москва</a></div>

                                        <div class="profile-cloud">
                                            <div class="profile-cloud-message"></div>
                                            <div class="profile-cloud-message-empty">Ваше приветствие или пара слов о себе.</div>
                                        </div>

                                        <ul class="profile-nav">
                                            <li class="active"><a href="#">Анкета знакомств</a></li>
                                            <li><a href="#">Альбомы</a></li>
                                            <li><a href="#">Объявления</a></li>
                                            <li><a href="#">Автопортрет</a></li>
                                            <li><a href="#">Мой дневник</a></li>
                                        </ul>
                                    </div>

                                </div>
                            </div>

                            <div class="profile-row">
                                <div class="profile-col">
                                    <div class="info-box">
                                        <i class="info-box-icon icon-present"></i>
                                        <div class="info-box-wrap">
                                            <div class="info-box-text">«Комплименты» дарят, когда хотят обратить на себя внимание. У вас пока нет ни одного.</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="profile-col">
                                    <div class="info-box">
                                        <i class="info-box-icon icon-vip"></i>
                                        <div class="info-box-wrap">
                                            <div class="info-box-text">Получите доступ к лучшим сервисам для удобного общения</div>
                                            <div class="info-box-action">
                                                <a href="#payment" class="btn-border btn-small btn-modal-payment">Активируйте <span class="hidden-xs hidden-sm hidden-md">супервозможности</span> </a>
                                                <a href="#" class="btn-question">?</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box-border profile-interest">

                                <h2>Мне нравиться</h2>

                                <ul class="usertags">
                                    <li>
                                        <a href="#" class="usertag"><span>Фантастика</span></a>
                                    </li>
                                    <li>
                                        <a href="#" class="usertag"><span>Путешествия</span></a>
                                    </li>
                                    <li>
                                        <a href="#" class="usertag"><span>Секс</span></a>
                                    </li>
                                </ul>

                                <div class="usertags-new">

                                    <a href="#" class="usertag-add"><b>+</b> Добавить интерес</a>

                                    <div class="usertags-list-wrap">

                                        <div class="usertags-list">

                                            <ul class="usertags-group">
                                                <li class="active" data-type=".category_01"><span>Интересы</span></li>
                                                <li data-type=".category_02"><span>Кино видео и TV</span></li>
                                                <li data-type=".category_03"><span>Спорт</span></li>
                                                <li data-type=".category_04"><span>Музыка</span></li>
                                                <li data-type=".category_05"><span>Путешествия</span></li>
                                                <li data-type=".category_06"><span>Сфера деятельнсти</span></li>
                                                <li data-type=".category_07"><span>Книги</span></li>
                                                <li data-type=".category_08"><span>Хобби</span></li>
                                                <li data-type=".category_09"><span>Еда и напитки</span></li>
                                                <li data-type=".category_10"><span>БДСМ</span></li>
                                            </ul>

                                            <div class="usertags-category">
                                                <ul class="category_01 active">
                                                    <li><a href="#">сон</a></li>
                                                    <li><a href="#">конфеты</a></li>
                                                    <li><a href="#">приключения</a></li>
                                                    <li><a href="#">воспитание детей</a></li>
                                                    <li><a href="#">женщины</a></li>
                                                    <li><a href="#">клубника</a></li>
                                                    <li><a href="#">звёзды</a></li>
                                                    <li><a href="#">лето</a></li>
                                                    <li><a href="#">жизнь</a></li>
                                                    <li><a href="#">чтение</a></li>
                                                    <li class="btn-reload-container"><a href="#" class="btn-reload"><i class="fa fa-refresh"></i> Показать другие</a></li>
                                                </ul>
                                                <ul class="category_02">
                                                    <li><a href="#">отечественные фильмы</a></li>
                                                    <li><a href="#">российские фильмы</a></li>
                                                    <li><a href="#">чёрно-белое кино</a></li>
                                                    <li><a href="#">молодежные комедии</a></li>
                                                    <li><a href="#">Джеймс Бонд</a></li>
                                                    <li><a href="#">психологические триллеры</a></li>
                                                    <li><a href="#">документальное кино</a></li>
                                                    <li><a href="#">мультфильмы</a></li>
                                                    <li><a href="#">эротическое кино</a></li>
                                                    <li><a href="#">семейное кино</a></li>
                                                    <li class="btn-reload-container"><a href="#" class="btn-reload"><i class="fa fa-refresh"></i> Показать другие</a></li>
                                                </ul>
                                                <ul class="category_03">
                                                    <li><a href="#">бодибилдинг</a></li>
                                                    <li><a href="#">айкидо</a></li>
                                                    <li><a href="#">мотокросс</a></li>
                                                    <li><a href="#">настольный теннис</a></li>
                                                    <li><a href="#">бег</a></li>
                                                    <li><a href="#">акробатика</a></li>
                                                    <li><a href="#">академическая гребля</a></li>
                                                    <li><a href="#">горный туризм</a></li>
                                                    <li><a href="#">спортивная ходьба</a></li>
                                                    <li><a href="#">атлетика</a></li>
                                                    <li class="btn-reload-container"><a href="#" class="btn-reload"><i class="fa fa-refresh"></i> Показать другие</a></li>
                                                </ul>
                                                <ul class="category_04">
                                                    <li><a href="#">трип-хоп</a></li>
                                                    <li><a href="#">народная музыка</a></li>
                                                    <li><a href="#">босса-нова</a></li>
                                                    <li><a href="#">трэш-метал</a></li>
                                                    <li><a href="#">реп</a></li>
                                                    <li><a href="#">хард-рок</a></li>
                                                    <li><a href="#">джаз</a></li>
                                                    <li><a href="#">рок</a></li>
                                                    <li><a href="#">фолк-рок</a></li>
                                                    <li><a href="#">брит-поп</a></li>
                                                    <li class="btn-reload-container"><a href="#" class="btn-reload"><i class="fa fa-refresh"></i> Показать другие</a></li>
                                                </ul>
                                                <ul class="category_05">
                                                    <li><a href="#">Панама</a></li>
                                                    <li><a href="#">Джибути</a></li>
                                                    <li><a href="#">Судан</a></li>
                                                    <li><a href="#">Таиланд</a></li>
                                                    <li><a href="#">Мьянма</a></li>
                                                    <li><a href="#">Тайвань</a></li>
                                                    <li><a href="#">Эритрея</a></li>
                                                    <li><a href="#">отдых на даче</a></li>
                                                    <li><a href="#">Йемен</a></li>
                                                    <li><a href="#">Ватикан</a></li>
                                                    <li class="btn-reload-container"><a href="#" class="btn-reload"><i class="fa fa-refresh"></i> Показать другие</a></li>
                                                </ul>
                                                <ul class="category_06">
                                                    <li><a href="#">бадминтон</a></li>
                                                    <li><a href="#">педагогика</a></li>
                                                    <li><a href="#">банковское дело</a></li>
                                                    <li><a href="#">бизнес</a></li>
                                                    <li><a href="#">сельское хозяйство</a></li>
                                                    <li><a href="#">хореография</a></li>
                                                    <li><a href="#">страхование</a></li>
                                                    <li><a href="#">фармацевтика</a></li>
                                                    <li><a href="#">макияж</a></li>
                                                    <li><a href="#">строительство</a></li>
                                                    <li class="btn-reload-container"><a href="#" class="btn-reload"><i class="fa fa-refresh"></i> Показать другие</a></li>
                                                </ul>
                                                <ul class="category_07">
                                                    <li><a href="#">классическая поэзия</a></li>
                                                    <li><a href="#">древнерусская литература</a></li>
                                                    <li><a href="#">историческая проза</a></li>
                                                    <li><a href="#">зарубежная фантастика</a></li>
                                                    <li><a href="#">зарубежные детективы</a></li>
                                                    <li><a href="#">военная литература</a></li>
                                                    <li><a href="#">мифы, эпос</a></li>
                                                    <li><a href="#">русская проза</a></li>
                                                    <li><a href="#">современная поэзия</a></li>
                                                    <li><a href="#">русская фантастика</a></li>
                                                    <li class="btn-reload-container"><a href="#" class="btn-reload"><i class="fa fa-refresh"></i> Показать другие</a></li>
                                                </ul>
                                                <ul class="category_08">
                                                    <li><a href="#">история</a></li>
                                                    <li><a href="#">путешествия</a></li>
                                                    <li><a href="#">фотография</a></li>
                                                    <li><a href="#">спорт</a></li>
                                                    <li><a href="#">гитара</a></li>
                                                    <li><a href="#">охота</a></li>
                                                    <li><a href="#">восточные танцы</a></li>
                                                    <li><a href="#">иностранные языки</a></li>
                                                    <li><a href="#">кулинария</a></li>
                                                    <li><a href="#">вокал</a></li>
                                                    <li class="btn-reload-container"><a href="#" class="btn-reload"><i class="fa fa-refresh"></i> Показать другие</a></li>
                                                </ul>
                                                <ul class="category_09">
                                                    <li><a href="#">Мясо</a></li>
                                                    <li><a href="#">Вино</a></li>
                                                    <li><a href="#">Азиатская кухня</a></li>
                                                    <li><a href="#">Виски</a></li>
                                                    <li><a href="#">Итальянская кухня</a></li>
                                                    <li><a href="#">Абсент</a></li>
                                                    <li><a href="#">Эль</a></li>
                                                    <li><a href="#">Японская кухня</a></li>
                                                    <li><a href="#">Крафтовое пиво</a></li>
                                                    <li><a href="#">Мартини</a></li>
                                                    <li class="btn-reload-container"><a href="#" class="btn-reload"><i class="fa fa-refresh"></i> Показать другие</a></li>
                                                </ul>
                                                <ul class="category_10">
                                                    <li><a href="#">Секс</a></li>
                                                    <li><a href="#">Флагеллация (порка)</a></li>
                                                    <li><a href="#">Дисциплина</a></li>
                                                    <li><a href="#">Золотой дождь</a></li>
                                                    <li><a href="#">Фистинг</a></li>
                                                    <li><a href="#">Игры с воском</a></li>
                                                    <li><a href="#">Садизм</a></li>
                                                    <li><a href="#">Унижение моральное</a></li>
                                                    <li><a href="#">Бондаж</a></li>
                                                    <li><a href="#">Мазохизм</a></li>
                                                    <li class="btn-reload-container"><a href="#" class="btn-reload"><i class="fa fa-refresh"></i> Показать другие</a></li>
                                                </ul>
                                            </div>

                                        </div>

                                        <div class="text-center">
                                            <a href="#" class="btn btn-md btn-usertag-add">Готово</a>
                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="box-border profile-question">

                                <div class="profile-question-header">
                                    <h2>Мои вопросы</h2>
                                    <a href="#" class="btn-add-question"><i class="fa fa-pencil" aria-hidden="true"></i> Добавить вопрос</a>
                                </div>

                                <div class="profile-question-new">
                                    <form class="profile-question-form">
                                        <div class="form-group">
                                            <textarea class="form-control" name="message" placeholder="Напишите вопрос" rows="3"></textarea>
                                        </div>
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-question-upload">Отправить</button>
                                            <button type="reset" class="btn btn-brown question-cancel">Отмена</button>
                                        </div>
                                    </form>
                                </div>

                                <div class="profile-question-content">

                                    <div class="question-intro">Если вы хотите, чтобы на вас обращали больше внимания, то присутствие вопросов на вашей странице - это один из возможных путей добиться этого. Задайте вопрос на интересующую вас тему и ждите ответов от других пользователей</div>

                                </div>



                            </div>

                            <div class="box-border profile-data">

                                <div class="profile-box profile-search">

                                    <div class="profile-data-header">
                                        <h2>Я ищу</h2>
                                        <a href="#" class="btn-data-edit"><i class="fa fa-pencil" aria-hidden="true"></i> Редактировать блок</a>
                                    </div>

                                    <div class="profile-data-new">
                                        <form class="profile-data-form">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="label-control">Познакомлюсь</label>
                                                        <ul>
                                                            <li>
                                                                <label class="input-checkbox">
                                                                    <input type="checkbox" name="sex" value="С девушкой">
                                                                    <span>девушку</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="input-checkbox">
                                                                    <input type="checkbox" name="sex" value="С парнем">
                                                                    <span>парню</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="input-checkbox">
                                                                    <input type="checkbox" name="sex" value="С парой">
                                                                    <span>пару</span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control">Возраст</label>
                                                        <ul>
                                                            <li>
                                                                <label class="input-checkbox">
                                                                    <input type="checkbox" name="age" value="18 - 25 лет">
                                                                    <span>18 - 25 лет</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="input-checkbox">
                                                                    <input type="checkbox" name="age" value="26 - 30 лет">
                                                                    <span>26 - 30 лет</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="input-checkbox">
                                                                    <input type="checkbox" name="age" value="31 - 35 лет">
                                                                    <span>31 - 40 лет</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="input-checkbox">
                                                                    <input type="checkbox" name="age" value="41 - 50 лет">
                                                                    <span>41 - 50 лет</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="input-checkbox">
                                                                    <input type="checkbox" name="age" value="51 - 60 лет">
                                                                    <span>51 - 60 лет</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="input-checkbox">
                                                                    <input type="checkbox" name="age" value="61 - 80 лет">
                                                                    <span>61 - 80 лет</span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label-control">Цель знакомства</label>
                                                    <ul>
                                                        <li>
                                                            <label class="input-checkbox">
                                                                <input type="checkbox" name="target" value="Дружба и общение">
                                                                <span>Дружба и общение</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="input-checkbox">
                                                                <input type="checkbox" name="target" value="Переписка">
                                                                <span>Переписка</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="input-checkbox">
                                                                <input type="checkbox" name="target" value="Отношения">
                                                                <span>Отношения</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="input-checkbox">
                                                                <input type="checkbox" name="target" value="Брак, создание семьи">
                                                                <span>Брак, создание семьи</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="input-checkbox">
                                                                <input type="checkbox" name="target" value="Флирт">
                                                                <span>Флирт</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="input-checkbox">
                                                                <input type="checkbox" name="target" value="Занятия спортом">
                                                                <span>Занятия спортом</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="input-checkbox">
                                                                <input type="checkbox" name="target" value="Путешествия">
                                                                <span>Путешествия</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="input-checkbox">
                                                                <input type="checkbox" name="target" value="Секс">
                                                                <span>Секс</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="input-checkbox">
                                                                <input type="checkbox" name="target" value="Встреча, Свидание">
                                                                <span>Встреча, Свидание</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="input-checkbox">
                                                                <input type="checkbox" name="target" value="Кого я хочу найти:">
                                                                <span>Кого я хочу найти:</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" class="btn profile-data-submit">Сохранить</button>
                                                <button type="reset" class="btn btn-brown profile-data-reset">Отмена</button>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="profile-box-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <strong>Познакомлюсь</strong>
                                                <ul>
                                                    <li>С девушкой 18-25 лет</li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6">
                                                <strong>Цель знакомства</strong>
                                                <ul>
                                                    <li>Дружба и общение</li>
                                                    <li>Брак, создание семьи</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="profile-box profile-about">

                                    <div class="profile-data-header">
                                        <h2>Обо мне</h2>
                                        <a href="#" class="btn-data-edit"><i class="fa fa-pencil" aria-hidden="true"></i> Редактировать блок</a>
                                    </div>

                                    <div class="profile-data-new">
                                        <form class="profile-data-form">
                                            <div class="row">

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="label-control">Ориентация:</label>
                                                        <select class="form-select">
                                                            <option value="">Пожалуста выберите</option>
                                                            <option value="">Гетеро</option>
                                                            <option value="">Би</option>
                                                            <option value="">Гей\Лесби</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control">рост:</label>
                                                        <select class="form-select">
                                                            <option value="">Пожалуста выберите</option>
                                                            <option value="">140</option>
                                                            <option value="">141</option>
                                                            <option value="">142</option>
                                                            <option value="">143</option>
                                                            <option value="">144</option>
                                                            <option value="">145</option>
                                                            <option value="">146</option>
                                                            <option value="">147</option>
                                                            <option value="">148</option>
                                                            <option value="">149</option>
                                                            <option value="">150</option>
                                                            <option value="">151</option>
                                                            <option value="">152</option>
                                                            <option value="">153</option>
                                                            <option value="">154</option>
                                                            <option value="">155</option>
                                                            <option value="">156</option>
                                                            <option value="">157</option>
                                                            <option value="">158</option>
                                                            <option value="">159</option>
                                                            <option value="">160</option>
                                                            <option value="">161</option>
                                                            <option value="">162</option>
                                                            <option value="">163</option>
                                                            <option value="">164</option>
                                                            <option value="">165</option>
                                                            <option value="">166</option>
                                                            <option value="">167</option>
                                                            <option value="">168</option>
                                                            <option value="">169</option>
                                                            <option value="">170</option>
                                                            <option value="">171</option>
                                                            <option value="">172</option>
                                                            <option value="">173</option>
                                                            <option value="">174</option>
                                                            <option value="">175</option>
                                                            <option value="">176</option>
                                                            <option value="">177</option>
                                                            <option value="">178</option>
                                                            <option value="">179</option>
                                                            <option value="">180</option>
                                                            <option value="">181</option>
                                                            <option value="">182</option>
                                                            <option value="">183</option>
                                                            <option value="">184</option>
                                                            <option value="">185</option>
                                                            <option value="">186</option>
                                                            <option value="">187</option>
                                                            <option value="">188</option>
                                                            <option value="">189</option>
                                                            <option value="">190</option>
                                                            <option value="">191</option>
                                                            <option value="">192</option>
                                                            <option value="">193</option>
                                                            <option value="">194</option>
                                                            <option value="">195</option>
                                                            <option value="">196</option>
                                                            <option value="">197</option>
                                                            <option value="">198</option>
                                                            <option value="">199</option>
                                                            <option value="">200</option>
                                                            <option value="">201</option>
                                                            <option value="">202</option>
                                                            <option value="">203</option>
                                                            <option value="">204</option>
                                                            <option value="">205</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control">Вес:</label>
                                                        <select class="form-select">
                                                            <option value="">Пожалуста выберите</option>
                                                            <option value="">45 кг</option>
                                                            <option value="">46 кг</option>
                                                            <option value="">47 кг</option>
                                                            <option value="">48 кг</option>
                                                            <option value="">49 кг</option>
                                                            <option value="">50 кг</option>
                                                            <option value="">51 кг</option>
                                                            <option value="">52 кг</option>
                                                            <option value="">53 кг</option>
                                                            <option value="">54 кг</option>
                                                            <option value="">55 кг</option>
                                                            <option value="">56 кг</option>
                                                            <option value="">57 кг</option>
                                                            <option value="">58 кг</option>
                                                            <option value="">59 кг</option>
                                                            <option value="">60 кг</option>
                                                            <option value="">61 кг</option>
                                                            <option value="">62 кг</option>
                                                            <option value="">63 кг</option>
                                                            <option value="">64 кг</option>
                                                            <option value="">65 кг</option>
                                                            <option value="">66 кг</option>
                                                            <option value="">67 кг</option>
                                                            <option value="">68 кг</option>
                                                            <option value="">69 кг</option>
                                                            <option value="">70 кг</option>
                                                            <option value="">71 кг</option>
                                                            <option value="">72 кг</option>
                                                            <option value="">73 кг</option>
                                                            <option value="">74 кг</option>
                                                            <option value="">75 кг</option>
                                                            <option value="">76 кг</option>
                                                            <option value="">77 кг</option>
                                                            <option value="">78 кг</option>
                                                            <option value="">79 кг</option>
                                                            <option value="">80 кг</option>
                                                            <option value="">81 кг</option>
                                                            <option value="">82 кг</option>
                                                            <option value="">83 кг</option>
                                                            <option value="">84 кг</option>
                                                            <option value="">85 кг</option>
                                                            <option value="">86 кг</option>
                                                            <option value="">87 кг</option>
                                                            <option value="">88 кг</option>
                                                            <option value="">89 кг</option>
                                                            <option value="">90 кг</option>
                                                            <option value="">91 кг</option>
                                                            <option value="">92 кг</option>
                                                            <option value="">93 кг</option>
                                                            <option value="">94 кг</option>
                                                            <option value="">95 кг</option>
                                                            <option value="">96 кг</option>
                                                            <option value="">97 кг</option>
                                                            <option value="">98 кг</option>
                                                            <option value="">99 кг</option>
                                                            <option value="">100 кг</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control">Телосложение:</label>
                                                        <select class="form-select">
                                                            <option value="">Пожалуста выберите</option>
                                                            <option value="">Худощавое</option>
                                                            <option value="">Обычное</option>
                                                            <option value="">Спортивное</option>
                                                            <option value="">Мускулистое</option>
                                                            <option value="">Плотное</option>
                                                            <option value="">Полное</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control">Внешность:</label>
                                                        <select class="form-select">
                                                            <option value="">Пожалуста выберите</option>
                                                            <option value="">Европейская</option>
                                                            <option value="">Азиатская</option>
                                                            <option value="">Кавказская</option>
                                                            <option value="">Индийская</option>
                                                            <option value="">Темнокожий</option>
                                                            <option value="">Испанская</option>
                                                            <option value="">Ближневосточная</option>
                                                            <option value="">Коренной американец</option>
                                                            <option value="">Смешанная</option>
                                                            <option value="">Другая</option>
                                                            <option value="">нет ответа</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control">Состоите ли вы в отношениях?</label>
                                                        <select class="form-select">
                                                            <option value="">Нет</option>
                                                            <option value="">Ничего серьёзного</option>
                                                            <option value="">Есть отношения</option>
                                                            <option value="">В браке</option>
                                                            <option value="">нет ответа</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">

                                                    <div class="form-group">
                                                        <label class="label-control">Отношение к курению:</label>
                                                        <select class="form-select">
                                                            <option value="">Пожалуста выберите</option>
                                                            <option value="">Не курю</option>
                                                            <option value="">Курю</option>
                                                            <option value="">Редко</option>
                                                            <option value="">Бросаю</option>
                                                            <option value="">нет ответа</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control">Отношение к алкоголю:</label>
                                                        <select class="form-select">
                                                            <option value="">Пожалуста выберите</option>
                                                            <option value="">Не пью вообще</option>
                                                            <option value="">Пью в компаниях изредка</option>
                                                            <option value="">Люблю выпить</option>
                                                            <option value="">нет ответа</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control">Дети:</label>
                                                        <select class="form-select">
                                                            <option value="">Нет</option>
                                                            <option value="">Нет, но хотелось бы</option>
                                                            <option value="">Есть, живём вместе</option>
                                                            <option value="">Есть, живём порознь</option>
                                                            <option value="">нет ответа</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control">Материальное положение:</label>
                                                        <select class="form-select">
                                                            <option value="">Непостоянные заработки</option>
                                                            <option value="">Постоянный небольшой доход</option>
                                                            <option value="">Стабильный средний доход</option>
                                                            <option value="">Хорошо зарабатываю / обеспечен</option>
                                                            <option value="">нет ответа</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control">Проживание:</label>
                                                        <select class="form-select">
                                                            <option value="">Отдельная квартира (снимаю или своя)</option>
                                                            <option value="">Комната в общежитии, коммуналка</option>
                                                            <option value="">Живу с родителями</option>
                                                            <option value="">Живу с приятелем / с подругой</option>
                                                            <option value="">Нет постоянного жилья</option>
                                                            <option value="">нет ответа</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-control">Знание языков:</label>
                                                        <select class="form-select">
                                                            <option value="">Русский (Russian)</option>
                                                            <option value="">English (English)</option>
                                                            <option value="">Deutsch (German)</option>
                                                            <option value="">Français (French)</option>
                                                            <option value="">Español (Spanish)</option>
                                                            <option value="">Italiano (Italian)</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" class="btn profile-data-submit">Сохранить</button>
                                                <button type="reset" class="btn btn-brown profile-data-reset">Отмена</button>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="profile-box-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="data-item">
                                                    <strong>Ориентация:</strong>
                                                    <p>Натурал</p>
                                                </div>
                                                <div class="data-item">
                                                    <strong>Внешность:</strong>
                                                    <p>Рост 147 см, вес 41 кг, телосложение стройное, цвет волос рыжие, цвет глаз голубые</p>
                                                </div>
                                                <div class="data-item">
                                                    <strong>Семья:</strong>
                                                    <p>Не имел семьи</p>
                                                </div>
                                                <div class="data-item">
                                                    <strong>Дети:</strong>
                                                    <p>Хочу детей</p>
                                                </div>
                                                <div class="data-item">
                                                    <strong>Курение:</strong>
                                                    <p>Иногда</p>
                                                </div>
                                                <div class="data-item">
                                                    <strong>Алкоголь:</strong>
                                                    <p>Не пью</p>
                                                </div>
                                                <div class="data-item">
                                                    <strong>Образование:</strong>
                                                    <p>Аспирантура</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="data-item">
                                                    <strong>Знак зодиака:</strong>
                                                    <p>Рак</p>
                                                </div>
                                                <div class="data-item">
                                                    <strong>Язык:</strong>
                                                    <p>English</p>
                                                </div>
                                                <div class="data-item">
                                                    <strong>Стаж в теме:</strong>
                                                    <p>Около 1 года</p>
                                                </div>
                                                <div class="data-item">
                                                    <strong>Есть место для сессий:</strong>
                                                    <p>Да</p>
                                                </div>
                                                <div class="data-item">
                                                    <strong>Рабочий график:</strong>
                                                    <p>Могу встречаться днем</p>
                                                </div>
                                                <div class="data-item">
                                                    <strong>Оплата встреч:</strong>
                                                    <p>Нет возможности спонсировать</p>
                                                </div>
                                                <div class="data-item">
                                                    <strong>Вид отношений:</strong>
                                                    <p>Мы пара и ищем девушку</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="profile-title">Могли бы вас заинтересовать</div>

                            <div class="people-gallery-small">
                                <div class="people-gallery-wrap">
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_03.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">2 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_02.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_05.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">4 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_04.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">1 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>

                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_03.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">2 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_02.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_04.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">1 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_05.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">4 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_01.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">3 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                    <div class="people-gallery-item">
                                        <a href="#">
                                            <img src="images/photo_03.jpg" class="img-responsive" alt="">
                                            <span class="people-gallery-text">2 <i class="fa fa-camera"></i></span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-left">

                            <div class="row">
                                <div class="col-sm-6 col-md-12">
                                    <div class="side-box">
                                        <div class="side-box-inner">
                                            <div class="side-box-title">Стандартные фоны:</div>
                                            <ul class="bg-list">
                                                <li class="active"><a href="#" class="theme_01" data-value="theme_01"></a></li>
                                                <li><a href="#" class="theme_02" data-value="theme_02"></a></li>
                                                <li><a href="#" class="theme_03" data-value="theme_03"></a></li>
                                                <li><a href="#" class="theme_04" data-value="theme_04"></a></li>
                                                <li><a href="#" class="theme_05" data-value="theme_05"></a></li>
                                                <li><a href="#" class="theme_06" data-value="theme_06"></a></li>
                                                <li><a href="#" class="theme_07" data-value="theme_07"></a></li>
                                                <li><a href="#" class="theme_08" data-value="theme_08"></a></li>
                                                <li><a href="#" class="theme_09" data-value="theme_09"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-12">
                                    <div class="side-box">
                                        <div class="side-super">
                                            <ul class="super-row">
                                                <li><a href="#payment" class="btn btn-modal-payment"><i class="fa fa-star"></i> Супервозможности</a></li>
                                                <li class="hidden-md"><a href="#" class="btn btn-brown btn-symbol"><i class="fa fa-question"></i></a></li>
                                            </ul>
                                            <div class="side-stats">
                                                <a href="#">
                                                    <span>Заходили к вам</span>
                                                    <b>3</b>
                                                </a>
                                                <a href="#">
                                                    <span>Чат</span>
                                                    <b>0</b>
                                                </a>
                                                <a href="#">
                                                    <span>Оценили ваши фото</span>
                                                    <b>2</b>
                                                </a>
                                                <a href="#">
                                                    <span>Друзья</span>
                                                    <b>10</b>
                                                </a>
                                                <a href="#">
                                                    <span>Вы нравитесь</span>
                                                    <b>12</b>
                                                </a>
                                                <a href="#">
                                                    <span>Взаимная симпатия</span>
                                                    <b>2</b>
                                                </a>
                                                <a href="#">
                                                    <span>Видят приватные фото</span>
                                                    <b>1</b>
                                                </a>
                                                <a href="#">
                                                    <span>Заблокированы</span>
                                                    <b>0</b>
                                                </a>
                                                <a href="#">
                                                    <span>Подмигнули</span>
                                                    <b>10</b>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
<link rel="stylesheet" href="js/vendor/ionRangeSlider/css/ion.rangeSlider.css">