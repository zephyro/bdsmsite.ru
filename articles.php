
<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">
                    <ul class="breadcrumbs">
                        <li><a href="#">Главная</a></li>
                        <li>Статьи</li>
                    </ul>

                    <div class="articles">

                        <div class="article-item">
                            <h4><a href="#"></a></h4>
                            <div class="article-content">
                                <div class="article-image">
                                    <div class="article-image-inner">
                                        <a href="#">
                                            <img src="images/article_01.jpg" class="img-responsive" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="article-text">
                                    <ul class="article-meta">
                                        <li>10.04.2017</li>
                                        <li>BDSMSite.ru</li>
                                        <li><a href="#">Общее</a></li>
                                    </ul>
                                    <p>Многие сабмиссивы идут в Тему с ощущением, что все будет как в их мечтах - идеально. Но прежде, чем сделать шаг, надо понять реальную картину, которая, порой очень далека от фантазий.</p>
                                    <a href="#">Добавить комментарии</a>
                                </div>
                            </div>
                        </div>

                        <div class="article-item">
                            <h4><a href="#"></a></h4>
                            <div class="article-content">
                                <div class="article-image">
                                    <div class="article-image-inner">
                                        <a href="#">
                                            <img src="images/article_02.jpg" class="img-responsive" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="article-text">
                                    <ul class="article-meta">
                                        <li>10.04.2017</li>
                                        <li>BDSMSite.ru</li>
                                        <li><a href="#">Личный опыт</a></li>
                                    </ul>
                                    <p>Моральная и сексуальная подготовка сабмиссива – настоящая миссия для Доминанта. Можно ли превратить процесс обучения нижней в ежедневную/еженедельную активность?</p>
                                    <a href="#">Добавить комментарии</a>
                                </div>
                            </div>
                        </div>

                        <div class="article-item">
                            <h4><a href="#"></a></h4>
                            <div class="article-content">
                                <div class="article-image">
                                    <div class="article-image-inner">
                                        <a href="#">
                                            <img src="images/article_03.jpg" class="img-responsive" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="article-text">
                                    <ul class="article-meta">
                                        <li>10.04.2017</li>
                                        <li>BDSMSite.ru</li>
                                        <li><a href="#">Психология</a></li>
                                    </ul>
                                    <p>Доминирование во время секса — это весело. Правда, иногда бывает так, что Доминант может переступить черту.</p>
                                    <a href="#">Добавить комментарии</a>
                                </div>
                            </div>
                        </div>

                        <div class="article-item">
                            <h4><a href="#"></a></h4>
                            <div class="article-content">
                                <div class="article-image">
                                    <div class="article-image-inner">
                                        <a href="#">
                                            <img src="images/article_04.jpg" class="img-responsive" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="article-text">
                                    <ul class="article-meta">
                                        <li>10.04.2017</li>
                                        <li>BDSMSite.ru</li>
                                        <li><a href="#">Общее</a></li>
                                    </ul>
                                    <p>Довольно сложно сделать так, чтобы девушке понравилось связывание, если она от этого не в восторге. Но все же это возможно.</p>
                                    <a href="#">Добавить комментарии</a>
                                </div>
                            </div>
                        </div>

                        <div class="article-item">
                            <h4><a href="#"></a></h4>
                            <div class="article-content">
                                <div class="article-image">
                                    <div class="article-image-inner">
                                        <a href="#">
                                            <img src="images/article_01.jpg" class="img-responsive" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="article-text">
                                    <ul class="article-meta">
                                        <li>10.04.2017</li>
                                        <li>BDSMSite.ru</li>
                                        <li><a href="#">Общее</a></li>
                                    </ul>
                                    <p>Многие сабмиссивы идут в Тему с ощущением, что все будет как в их мечтах - идеально. Но прежде, чем сделать шаг, надо понять реальную картину, которая, порой очень далека от фантазий.</p>
                                    <a href="#">Добавить комментарии</a>
                                </div>
                            </div>
                        </div>

                        <div class="article-item">
                            <h4><a href="#"></a></h4>
                            <div class="article-content">
                                <div class="article-image">
                                    <div class="article-image-inner">
                                        <a href="#">
                                            <img src="images/article_02.jpg" class="img-responsive" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="article-text">
                                    <ul class="article-meta">
                                        <li>10.04.2017</li>
                                        <li>BDSMSite.ru</li>
                                        <li><a href="#">Личный опыт</a></li>
                                    </ul>
                                    <p>Моральная и сексуальная подготовка сабмиссива – настоящая миссия для Доминанта. Можно ли превратить процесс обучения нижней в ежедневную/еженедельную активность?</p>
                                    <a href="#">Добавить комментарии</a>
                                </div>
                            </div>
                        </div>

                        <div class="article-item">
                            <h4><a href="#"></a></h4>
                            <div class="article-content">
                                <div class="article-image">
                                    <div class="article-image-inner">
                                        <a href="#">
                                            <img src="images/article_03.jpg" class="img-responsive" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="article-text">
                                    <ul class="article-meta">
                                        <li>10.04.2017</li>
                                        <li>BDSMSite.ru</li>
                                        <li><a href="#">Психология</a></li>
                                    </ul>
                                    <p>Доминирование во время секса — это весело. Правда, иногда бывает так, что Доминант может переступить черту.</p>
                                    <a href="#">Добавить комментарии</a>
                                </div>
                            </div>
                        </div>

                    </div>

                    <ul class="pagination">
                        <li><span>1</span></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">6</a></li>
                        <li><a href="#">7</a></li>
                    </ul>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
