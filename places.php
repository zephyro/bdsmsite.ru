
<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div class="top-line">
                        <ul class="breadcrumbs">
                            <li><a href="#">Главная</a></li>
                            <li>Заведения</li>
                        </ul>

                        <div class="top-search">
                            <form class="form">
                                <input type="text" class="form-control form-control-small" name="search" placeholder="Поиск">
                                <button type="submit" class="btn btn-small"><i class="fa fa-search"></i> Поиск</button>
                            </form>
                        </div>

                    </div>

                    <div class="event-line">
                        <ul class="event-nav">
                            <li><a href="#">Клубы</a></li>
                            <li><a href="#">Концерты</a></li>
                            <li><a href="#">Мастер-классы</a></li>
                            <li><a href="#">Отели</a></li>
                            <li><a href="#">БДСМ-студии</a></li>
                            <li><a href="#">Бары</a></li>
                            <li><a href="#">Фотостудии</a></li>
                        </ul>

                        <div class="action-event-new">
                            <a href="#" class="btn btn-small"><i class="fa fa-pencil"></i> Добавить отзыв</a>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-8 col-lg-8">

                            <div class="event-box">

                                <div class="event-heading">
                                    <h2>Рекомендуем</h2>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="place">
                                            <div class="place-image">
                                                <a href="#">
                                                    <img src="img/no_image_sm.gif" class="img-responsive" alt="">
                                                </a>
                                            </div>
                                            <div class="place-content">
                                                <a href="#" class="place-name">Клуб свободных отношений "Красавица и Чудовище"</a>
                                                <div class="place-info">Санкт-Петербург, 0 отзывов</div>
                                                <div class="rate">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="place">
                                            <div class="place-image">
                                                <a href="#">
                                                    <img src="img/no_image_sm.gif" class="img-responsive" alt="">
                                                </a>
                                            </div>
                                            <div class="place-content">
                                                <a href="#" class="place-name">Клуб свободных отношений "Красавица и Чудовище"</a>
                                                <div class="place-info">Санкт-Петербург, 0 отзывов</div>
                                                <div class="rate">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="event-box">

                                <div class="event-heading">
                                    <h2>Лучшие места недели</h2>
                                </div>

                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="place place-md">
                                            <div class="place-image">
                                                <a href="#">
                                                    <img src="img/no_image_sm.gif" class="img-responsive" alt="">
                                                </a>
                                            </div>
                                            <div class="place-content">
                                                <a href="#" class="place-name">Клуб свободных отношений "Красавица и Чудовище"</a>
                                                <div class="place-info">Санкт-Петербург, 0 отзывов</div>
                                                <div class="rate">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                                <div class="place-tags">
                                                    <a href="#">БДСМ-студии</a>, <a href="#">Москва</a>, <a href="#">Досуг</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="place place-md">
                                            <div class="place-image">
                                                <a href="#">
                                                    <img src="img/no_image_sm.gif" class="img-responsive" alt="">
                                                </a>
                                            </div>
                                            <div class="place-content">
                                                <a href="#" class="place-name">Клуб свободных отношений "Красавица и Чудовище"</a>
                                                <div class="place-info">Санкт-Петербург, 0 отзывов</div>
                                                <div class="rate">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                                <div class="place-tags">
                                                    <a href="#">БДСМ-студии</a>, <a href="#">Москва</a>, <a href="#">Досуг</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="place place-md">
                                            <div class="place-image">
                                                <a href="#">
                                                    <img src="img/no_image_sm.gif" class="img-responsive" alt="">
                                                </a>
                                            </div>
                                            <div class="place-content">
                                                <a href="#" class="place-name">Клуб свободных отношений "Красавица и Чудовище"</a>
                                                <div class="place-info">Санкт-Петербург, 0 отзывов</div>
                                                <div class="rate">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                                <div class="place-tags">
                                                    <a href="#">БДСМ-студии</a>, <a href="#">Москва</a>, <a href="#">Досуг</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="place place-md">
                                            <div class="place-image">
                                                <a href="#">
                                                    <img src="img/no_image_sm.gif" class="img-responsive" alt="">
                                                </a>
                                            </div>
                                            <div class="place-content">
                                                <a href="#" class="place-name">Клуб свободных отношений "Красавица и Чудовище"</a>
                                                <div class="place-info">Санкт-Петербург, 0 отзывов</div>
                                                <div class="rate">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                                <div class="place-tags">
                                                    <a href="#">БДСМ-студии</a>, <a href="#">Москва</a>, <a href="#">Досуг</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <div class="col-md-4 col-lg-4">
                            <div class="side-box">
                                <div class="side-box-heading">Отзыв дня</div>
                                <div class="side-box-inner">

                                    <div class="place place-sm">
                                        <div class="place-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" class="img-responsive" alt="">
                                            </a>
                                        </div>
                                        <div class="place-content">
                                            <a href="#" class="place-name">Клуб свободных отношений "Красавица и Чудовище"</a>
                                            <div class="place-info">Санкт-Петербург, 1 отзыв</div>
                                            <div class="rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="place-review-author">От <a href="#"">Karlson</a></div>
                                        </div>
                                    </div>




                                </div>
                                <div class="side-box-heading">Популярные</div>
                                <div class="side-box-inner">

                                    <div class="place place-sm">
                                        <div class="place-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" class="img-responsive" alt="">
                                            </a>
                                        </div>
                                        <div class="place-content">
                                            <a href="#" class="place-name">Клуб свободных отношений "Красавица и Чудовище"</a>
                                            <div class="place-info">Санкт-Петербург, 1 отзыв</div>
                                            <div class="rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="place-review-author">От <a href="#"">Karlson</a></div>
                                        </div>
                                    </div>

                                    <div class="place place-sm">
                                        <div class="place-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" class="img-responsive" alt="">
                                            </a>
                                        </div>
                                        <div class="place-content">
                                            <a href="#" class="place-name">Клуб свободных отношений "Красавица и Чудовище"</a>
                                            <div class="place-info">Санкт-Петербург, 1 отзыв</div>
                                            <div class="rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="place-review-author">От <a href="#"">Karlson</a></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
