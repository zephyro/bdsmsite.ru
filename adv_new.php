<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div class="row">

                        <div class="col-right">

                            <div class="box-border">
                                <div class="box-border-header icon-header"><i class="fa fa-pencil-square-o"></i> Новое объявление</div>

                                <div class="announcement-new">

                                    <div class="form-warning">
                                        <strong>Совершенно конфиденциально</strong><br/>
                                        Ваш профиль в Единой Службе Знакомств и Общения не будет виден никому из тех, кто просматривает объявления.
                                    </div>

                                    <form class="form">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="label-control">Кого ищете:</div>
                                                    <select class="form-select">
                                                        <option value="">Девушку</option>
                                                        <option value="">Парня</option>
                                                        <option value="">Пару</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <div class="label-control">Цель знакомства:</div>
                                                    <select class="form-select">
                                                        <option value="">Не важно</option>
                                                        <option value="">Дружба и переписка</option>
                                                        <option value="">Секс</option>
                                                        <option value="">Романтика и серьезные отношения</option>
                                                        <option value="">Совместные путешествия</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-control">Электронная почта:</label>
                                            <input type="text" class="form-control" name="email" placeholder="mail@email.com">
                                        </div>
                                        <div class="form-group">
                                            <label class="label-control">Текст объявления:</label>
                                            <textarea class="form-control" name="meassage" placeholder="" rows="6"></textarea>
                                        </div>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-md">Отправить</button>
                                        </div>
                                    </form>

                                </div>

                            </div>

                        </div>

                        <div class="col-left">

                            <div class="side-box">
                                <div class="side-box-inner">

                                    <div class="text-center">
                                        <a href="#" class="btn-text">Мои объявления</a>
                                    </div>

                                    <div class="text-center">
                                        <a class="btn btn-md" href="#">Подать обьявление</a>
                                    </div>
                                    <br/>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
<link rel="stylesheet" href="js/vendor/ionRangeSlider/css/ion.rangeSlider.css">