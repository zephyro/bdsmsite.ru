<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.auth.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div title="row">

                        <div class="col-md-4 col-lg-4">

                            <a href="#" class="go-back"><i class="fa fa-backward" aria-hidden="true"></i> <span>Назад</span></a>

                        </div>

                        <div class="col-md-8 col-lg-8">

                            <div class="registration">

                                <h2>Ваш БДСМ аккаунт!</h2>

                                <div class="row">
                                    <div class="col-sm-7 col-md-7">

                                        <div class="reg-form">
                                            <div class="reg-title">Для регистрации заполните эту форму:</div>
                                            <form class="form">

                                                <div class="form-row">
                                                    <label class="form-label">Почта:</label>
                                                    <div class="form-group">
                                                        <input class="form-control" type="text" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <label class="form-label">Пароль:</label>
                                                    <div class="form-group">
                                                        <input class="form-control" type="text" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <label class="form-label">Имя:</label>
                                                    <div class="form-group">
                                                        <input class="form-control" type="text" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <label class="form-label">Год рождения:</label>
                                                    <div class="form-group">
                                                        <select class="form-select">
                                                            <option value="">1970</option>
                                                            <option value="">1971</option>
                                                            <option value="">1972</option>
                                                            <option value="">1973</option>
                                                            <option value="">1974</option>
                                                            <option value="">1975</option>
                                                            <option value="">1976</option>
                                                            <option value="">1977</option>
                                                            <option value="">1978</option>
                                                            <option value="">1979</option>
                                                            <option value="">1980</option>
                                                            <option value="">1981</option>
                                                            <option value="">1982</option>
                                                            <option value="">1983</option>
                                                            <option value="">1984</option>
                                                            <option value="">1985</option>
                                                            <option value="">1986</option>
                                                            <option value="">1987</option>
                                                            <option value="">1988</option>
                                                            <option value="">1989</option>
                                                            <option value="">1990</option>
                                                            <option value="">1991</option>
                                                            <option value="">1992</option>
                                                            <option value="">1993</option>
                                                            <option value="">1994</option>
                                                            <option value="">1995</option>
                                                            <option value="">1996</option>
                                                            <option value="">1997</option>
                                                            <option value="">1998</option>
                                                            <option value="">1999</option>
                                                            <option value="">2000</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <label class="form-label">Кто я:</label>
                                                    <div class="form-group">
                                                        <label class="checkbox-inline">
                                                            <input type="radio" name="sex" checked>
                                                            <span>Муж.</span>
                                                        </label>
                                                        <label class="checkbox-inline">
                                                            <input type="radio" name="sex">
                                                            <span>Жен.</span>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <label class="form-label">Роль:</label>
                                                    <div class="form-group">
                                                        <label class="checkbox-inline">
                                                            <input type="radio" name="role" checked>
                                                            <span>Низ.</span>
                                                        </label>
                                                        <label class="checkbox-inline">
                                                            <input type="radio" name="role">
                                                            <span>Верх.</span>
                                                        </label>
                                                        <label class="checkbox-inline">
                                                            <input type="radio" name="role">
                                                            <span>Свитч.</span>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <label class="form-label">Город:</label>
                                                    <div class="form-group">
                                                        <div class="reg-city">
                                                            <span class="user-city">Москва</span> <a href="#" class="btn-city-select">Изменить</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <label class="form-label">Код:</label>
                                                    <div class="form-group">
                                                        <img src="images/captcha.jpg" class="img-responsive">
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <label class="form-label hidden-xs"></label>
                                                    <div class="form-group">
                                                        <label class="input-checkbox">
                                                            <input type="checkbox" name="goods">
                                                            <span>Я принимаю <a href="#">условия</a> и <a href="#">Политику безопасности</a></span>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <label class="form-label hidden-xs"></label>
                                                    <div class="form-group text-center">
                                                        <button type="submit" class="btn">Регистрация</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>

                                    </div>
                                    <div class="col-sm-5 col-md-5">

                                        <div class="reg-social">
                                            <div class="reg-title">или войдите через социальную сеть</div>
                                            <div class="reg-social-list">
                                                <a href="#" title="Войти через Facebook"><i class="fa fa-facebook"></i></a>
                                                <a href="#" title="Войти через Одноклассники"><i class="fa fa-odnoklassniki"></i></a>
                                                <a href="#" title="Войти через Вконтакте"><i class="fa fa-vk"></i></a>
                                                <a href="#" title="Войти через Google"><i class="fa fa-google"></i></a>
                                                <a href="#" title="Войти через Yandex"><i class="yandex">Я</i></a>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>


                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
