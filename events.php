
<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div class="top-line">
                        <ul class="breadcrumbs">
                            <li><a href="#">Главная</a></li>
                            <li>События</li>
                        </ul>

                        <div class="top-search">
                            <form class="form">
                                <input type="text" class="form-control form-control-small" name="search" placeholder="Поиск">
                                <button type="submit" class="btn btn-small"><i class="fa fa-search"></i> Поиск</button>
                            </form>
                        </div>

                    </div>

                    <div class="event-line">
                        <ul class="event-nav">
                            <li><a href="#">Все Вечеринки</a></li>
                            <li><a href="#">Семинары</a></li>
                            <li><a href="#">Мастер-классы</a></li>
                            <li><a href="#">Закрытые мероприятия</a></li>
                            <li><a href="#">Неформальные встречи</a></li>
                        </ul>

                        <div class="action-event-new">
                            <a href="#" class="btn btn-small"><i class="fa fa-pencil"></i> Создать</a>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-8 col-lg-8">

                            <div class="event-box">

                                <div class="event-heading">
                                    <h2>Предстоящие</h2>
                                    <a class="btn btn-brown btn-small" href="#"><i class="fa fa-eye"></i> смотреть все</a>
                                </div>

                                <ul class="event-list">

                                    <li>
                                        <a href="#" class="event-image">
                                            <img src="img/no_image_sm.gif" alt="" title="" class="img-responsive">
                                        </a>
                                        <div class="event-name"><a href="#">Длинное название мероприятия</a></div>
                                        <ul class="event-activity">
                                            <li>Гости: 9280</li>
                                            <li>Коммент.: 0</li>
                                        </ul>
                                        <ul class="event-meta">
                                            <li><a href="#">Sixty</a></li>
                                            <li><a href="#">24 Янв 2018</a></li>
                                            <li><span>11:00</span></li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="#" class="event-image">
                                            <img src="img/no_image_sm.gif" alt="" title="" class="img-responsive">
                                        </a>
                                        <div class="event-name"><a href="#">Длинное название мероприятия</a></div>
                                        <ul class="event-activity">
                                            <li>Гости: 9280</li>
                                            <li>Коммент.: 0</li>
                                        </ul>
                                        <ul class="event-meta">
                                            <li><a href="#">Якитория</a></li>
                                            <li><a href="#">24 Янв 2018</a></li>
                                            <li><span>11:00</span></li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="#" class="event-image">
                                            <img src="img/no_image_sm.gif" alt="" title="" class="img-responsive">
                                        </a>
                                        <div class="event-name"><a href="#">Длинное название мероприятия</a></div>
                                        <ul class="event-activity">
                                            <li>Гости: 9280</li>
                                            <li>Коммент.: 0</li>
                                        </ul>
                                        <ul class="event-meta">
                                            <li><a href="#">Карлсон</a></li>
                                            <li><a href="#">24 Янв 2018</a></li>
                                            <li><span>11:00</span></li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="#" class="event-image">
                                            <img src="img/no_image_sm.gif" alt="" title="" class="img-responsive">
                                        </a>
                                        <div class="event-name"><a href="#">Длинное название мероприятия</a></div>
                                        <ul class="event-activity">
                                            <li>Гости: 9280</li>
                                            <li>Коммент.: 0</li>
                                        </ul>
                                        <ul class="event-meta">
                                            <li><a href="#">Sixty</a></li>
                                            <li><a href="#">24 Янв 2018</a></li>
                                            <li><span>11:00</span></li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="#" class="event-image">
                                            <img src="img/no_image_sm.gif" alt="" title="" class="img-responsive">
                                        </a>
                                        <div class="event-name"><a href="#">Длинное название мероприятия</a></div>
                                        <ul class="event-activity">
                                            <li>Гости: 9280</li>
                                            <li>Коммент.: 0</li>
                                        </ul>
                                        <ul class="event-meta">
                                            <li><a href="#">Якитория</a></li>
                                            <li><a href="#">24 Янв 2018</a></li>
                                            <li><span>11:00</span></li>
                                        </ul>
                                    </li>

                                </ul>

                            </div>

                            <div class="event-box">

                                <div class="event-heading">
                                    <h2>Самые ожидаемые</h2>
                                    <a class="btn btn-brown btn-small" href="#"><i class="fa fa-eye"></i> смотреть все</a>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-striped post-table">
                                        <tr>
                                            <th></th>
                                            <th>Событие</th>
                                            <th>Где</th>
                                            <th>Когда.</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#" class="post-table-avatar">
                                                    <img src="img/no-photo.png" alt="" title="" class="img-responsive">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="#">Очень интресное мероприятие</a>
                                            </td>
                                            <td>
                                                <a href="#">Sixty</a>
                                            </td>
                                            <td><a href="#">24 Янв 2018</a> | 11:00</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#" class="post-table-avatar">
                                                    <img src="img/no-photo.png" alt="" title="" class="img-responsive">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="#">Самое интресное мероприятие</a>
                                            </td>
                                            <td>
                                                <a href="#">Sixty</a>
                                            </td>
                                            <td><a href="#">25 Янв 2018</a> | 11:00</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#" class="post-table-avatar">
                                                    <img src="img/no-photo.png" alt="" title="" class="img-responsive">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="#">Очень интресное мероприятие</a>
                                            </td>
                                            <td>
                                                <a href="#">Sixty</a>
                                            </td>
                                            <td><a href="#">24 Янв 2018</a> | 11:00</td>
                                        </tr>

                                    </table>
                                </div>

                            </div>

                        </div>
                        <div class="col-md-4 col-lg-4">
                            <div class="side-box">
                                <div class="side-box-heading">Обсуждаемые</div>
                                <div class="side-box-inner">

                                    <div class="event-discuss">
                                        <div class="event-discuss-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" alt="" title="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="event-discuss-content">
                                            <a href="#" class="event-name">Очень интересное мероприятие</a>
                                            <ul class="event-meta">
                                                <li>Гости: 1</li>
                                                <li>Коммент.: 0</li>
                                                <li><a href="#">социальная сеть</a></li>
                                                <li><a href="#">24 Янв 2018</a> | 11:00</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="event-discuss">
                                        <div class="event-discuss-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" alt="" title="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="event-discuss-content">
                                            <a href="#" class="event-name">Сомое лучщее интересное мероприятие</a>
                                            <ul class="event-meta">
                                                <li>Гости: 105</li>
                                                <li>Коммент.: 20</li>
                                                <li><a href="#">социальная сеть</a></li>
                                                <li><a href="#">24 Янв 2018</a> | 11:00</li>
                                            </ul>
                                        </div>
                                    </div>



                                </div>
                                <div class="side-box-heading">Прошедшие</div>
                                <div class="side-box-inner">

                                    <div class="event-discuss">
                                        <div class="event-discuss-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" alt="" title="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="event-discuss-content">
                                            <a href="#" class="event-name">Очень интересное мероприятие</a>
                                            <ul class="event-meta">
                                                <li>Гости: 1</li>
                                                <li>Коммент.: 0</li>
                                                <li><a href="#">социальная сеть</a></li>
                                                <li><a href="#">24 Янв 2018</a> | 11:00</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="event-discuss">
                                        <div class="event-discuss-image">
                                            <a href="#">
                                                <img src="img/no_image_sm.gif" alt="" title="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="event-discuss-content">
                                            <a href="#" class="event-name">Сомое лучщее интересное мероприятие</a>
                                            <ul class="event-meta">
                                                <li>Гости: 105</li>
                                                <li>Коммент.: 20</li>
                                                <li><a href="#">социальная сеть</a></li>
                                                <li><a href="#">24 Янв 2018</a> | 11:00</li>
                                            </ul>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
