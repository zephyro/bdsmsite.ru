
<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- PhotoLine -->
            <?php include('inc/photoline.inc.php') ?>
            <!-- -->

            <!-- UserBar -->
            <?php include('inc/userbar.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="container">

                    <div class="top-line">
                        <ul class="breadcrumbs">
                            <li><a href="#">Главная</a></li>
                            <li>Блоги</li>
                        </ul>

                        <div class="top-search">
                            <form class="form">
                                <input type="text" class="form-control form-control-small" name="search" placeholder="Поиск">
                                <button type="submit" class="btn btn-small"><i class="fa fa-search"></i> Поиск</button>
                            </form>
                        </div>

                    </div>

                    <div class="event-line">
                        <ul class="event-nav">
                            <li><a href="#">Мой блог</a></li>
                            <li><a href="#">Блоги Друзей</a></li>
                            <li><a href="#">Мои подписки</a></li>
                        </ul>

                        <div class="action-event-new">
                            <a href="#" class="btn btn-small"><i class="fa fa-pencil"></i> Написать</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8 col-lg-8">

                            <div class="event-box">

                                <div class="event-heading">
                                    <h2>Топ блоггеров</h2>
                                    <a class="btn btn-brown btn-small" href="#"><i class="fa fa-eye"></i> смотреть все</a>
                                </div>

                                <ul class="blogger-list">

                                    <li>
                                        <a href="#" class="blogger-avatar">
                                            <img src="images/photo_01.jpg" alt="" title="" class="img-responsive">
                                        </a>
                                        <div class="blogger-name"><a href="#">User Name</a></div>
                                        <a class="btn btn-small" href="#">Читать</a>
                                        <span class="blogger-meta">Гости: 9280</span>
                                        <span class="blogger-meta">Коммент.: 0</span>
                                    </li>

                                    <li>
                                        <a href="#" class="blogger-avatar">
                                            <img src="images/photo_02.jpg" alt="" title="" class="img-responsive">
                                        </a>
                                        <div class="blogger-name"><a href="#">Large User Name</a></div>
                                        <a class="btn btn-small" href="#">Читать</a>
                                        <span class="blogger-meta">Гости: 9280</span>
                                        <span class="blogger-meta">Коммент.: 0</span>
                                    </li>

                                    <li>
                                        <a href="#" class="blogger-avatar">
                                            <img src="images/photo_03.jpg" alt="" title="" class="img-responsive">
                                        </a>
                                        <div class="blogger-name"><a href="#">Large User Name</a></div>
                                        <a class="btn btn-small" href="#">Читать</a>
                                        <span class="blogger-meta">Гости: 9280</span>
                                        <span class="blogger-meta">Коммент.: 0</span>
                                    </li>

                                    <li>
                                        <a href="#" class="blogger-avatar">
                                            <img src="images/photo_01.jpg" alt="" title="" class="img-responsive">
                                        </a>
                                        <div class="blogger-name"><a href="#">User Name</a></div>
                                        <a class="btn btn-small" href="#">Читать</a>
                                        <span class="blogger-meta">Гости: 9280</span>
                                        <span class="blogger-meta">Коммент.: 0</span>
                                    </li>

                                    <li>
                                        <a href="#" class="blogger-avatar">
                                            <img src="images/photo_02.jpg" alt="" title="" class="img-responsive">
                                        </a>
                                        <div class="blogger-name"><a href="#">Large User Name</a></div>
                                        <a class="btn btn-small" href="#">Читать</a>
                                        <span class="blogger-meta">Гости: 9280</span>
                                        <span class="blogger-meta">Коммент.: 0</span>
                                    </li>

                                    <li>
                                        <a href="#" class="blogger-avatar">
                                            <img src="images/photo_03.jpg" alt="" title="" class="img-responsive">
                                        </a>
                                        <div class="blogger-name"><a href="#">Large User Name</a></div>
                                        <a class="btn btn-small" href="#">Читать</a>
                                        <span class="blogger-meta">Гости: 9280</span>
                                        <span class="blogger-meta">Коммент.: 0</span>
                                    </li>

                                </ul>

                            </div>

                            <div class="event-box">

                                <div class="event-heading">
                                    <h2>Популярные посты</h2>
                                    <a class="btn btn-brown btn-small" href="#"><i class="fa fa-eye"></i> смотреть все</a>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-striped post-table">
                                        <tr>
                                            <th></th>
                                            <th>Название поста</th>
                                            <th>Блоггер</th>
                                            <th>Просм.</th>
                                            <th>Коммент.</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#" class="post-table-avatar">
                                                    <img src="images/photo_01.jpg" alt="" title="" class="img-responsive">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="#">Строгая госпожа ищет нижнего для ви...</a>
                                            </td>
                                            <td>
                                                <a href="#">карина7</a>
                                            </td>
                                            <td>199</td>
                                            <td>4</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#" class="post-table-avatar">
                                                    <img src="images/photo_02.jpg" alt="" title="" class="img-responsive">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="#">Уважаемые любители попиариться</a>
                                            </td>
                                            <td>
                                                <a href="#">Мила</a>
                                            </td>
                                            <td>3550</td>
                                            <td>120</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#" class="post-table-avatar">
                                                    <img src="images/photo_03.jpg" alt="" title="" class="img-responsive">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="#">Предвкушение</a>
                                            </td>
                                            <td>
                                                <a href="#">УникальныйВерх</a>
                                            </td>
                                            <td>234</td>
                                            <td>1</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#" class="post-table-avatar">
                                                    <img src="images/photo_04.jpg" alt="" title="" class="img-responsive">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="#">Скорее психологический аспект )</a>
                                            </td>
                                            <td>
                                                <a href="#">zhupel</a>
                                            </td>
                                            <td>78</td>
                                            <td>1</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#" class="post-table-avatar">
                                                    <img src="images/photo_03.jpg" alt="" title="" class="img-responsive">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="#">О вреде мастурбации и онанизма</a>
                                            </td>
                                            <td>
                                                <a href="#">Психоаналитик</a>
                                            </td>
                                            <td>234</td>
                                            <td>1</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#" class="post-table-avatar">
                                                    <img src="images/photo_03.jpg" alt="" title="" class="img-responsive">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="#">Строгая госпожа ищет нижнего</a>
                                            </td>
                                            <td>
                                                <a href="#">карина7</a>
                                            </td>
                                            <td>234</td>
                                            <td>1</td>
                                        </tr>
                                    </table>
                                </div>

                            </div>

                        </div>
                        <div class="col-md-4 col-lg-4">
                            <div class="side-box">
                                <div class="side-box-heading">Новые блоги</div>
                                <div class="side-box-inner">

                                    <ul class="blog-new">
                                        <li>
                                            <strong><a href="#">Кирюха</a></strong> <span class="blog-new-date">10.01.18</span>
                                            <p>сли вы хотите, чтобы на вас обращали больше внимания, то присутствие вопросов на вашей странице</p>
                                        </li>
                                        <li>
                                            <strong><a href="#">карина7</a></strong> <span class="blog-new-date">10.01.18</span>
                                            <p>сли вы хотите, чтобы на вас обращали больше внимания, то присутствие вопросов на вашей странице</p>
                                        </li>
                                        <li>
                                            <strong><a href="#">карина7</a></strong> <span class="blog-new-date">10.01.18</span>
                                            <p>сли вы хотите, чтобы на вас обращали больше внимания, то присутствие вопросов на вашей странице</p>
                                        </li>
                                        <li>
                                            <strong><a href="#">zhupel</a></strong> <span class="blog-new-date">10.01.18</span>
                                            <p>сли вы хотите, чтобы на вас обращали больше внимания, то присутствие вопросов на вашей странице</p>
                                        </li>
                                        <li>
                                            <strong><a href="#">УникальныйВерх</a></strong> <span class="blog-new-date">10.01.18</span>
                                            <p>сли вы хотите, чтобы на вас обращали больше внимания, то присутствие вопросов на вашей странице</p>
                                        </li>
                                        <li>
                                            <strong><a href="#">Психоаналитик</a></strong> <span class="blog-new-date">10.01.18</span>
                                            <p>сли вы хотите, чтобы на вас обращали больше внимания, то присутствие вопросов на вашей странице</p>
                                        </li>
                                    </ul>

                                    <div class="text-right">
                                        <a class="btn btn-brown btn-small"><i class="fa fa-eye"></i> Смотреть все</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
